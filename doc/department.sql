/*
Navicat MySQL Data Transfer

Source Server         : 183.224.41.189
Source Server Version : 50717
Source Host           : 183.224.41.189:8087
Source Database       : ContractMS

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-01-06 11:17:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `deptID` varchar(100) NOT NULL COMMENT '部门ID（时间戳+随机数4位）',
  `deptName` varchar(100) NOT NULL COMMENT '部门名称',
  `deptDes` varchar(500) DEFAULT NULL COMMENT '部门描述',
  `deptStatus` smallint(5) unsigned DEFAULT '1' COMMENT '部门状态（1可用，0禁用）',
  `deptCTime` datetime DEFAULT NULL COMMENT '部门创建时间',
  `deptUTime` datetime DEFAULT NULL COMMENT '部门更新时间',
  `deptDTime` datetime DEFAULT NULL COMMENT '部门删除时间',
  PRIMARY KEY (`deptID`),
  UNIQUE KEY `department_pk` (`deptName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('1', '总经理', null, '1', null, null, null);
INSERT INTO `department` VALUES ('10', '互联网渠道事业部', null, '1', null, null, null);
INSERT INTO `department` VALUES ('11', '集中运营事业部', null, '1', null, null, null);
INSERT INTO `department` VALUES ('12', '生活服务事业部', null, '1', null, null, null);
INSERT INTO `department` VALUES ('13', '网上商城事业部', null, '1', null, null, null);
INSERT INTO `department` VALUES ('2', '副总经理', null, '1', null, null, null);
INSERT INTO `department` VALUES ('3', '行政部', null, '1', null, null, null);
INSERT INTO `department` VALUES ('4', '财政部', null, '1', null, null, null);
INSERT INTO `department` VALUES ('5', '人力资源部', null, '1', null, null, null);
INSERT INTO `department` VALUES ('6', '研发部', null, '1', null, null, null);
INSERT INTO `department` VALUES ('7', '彩云优品项目组', null, '1', null, null, null);
INSERT INTO `department` VALUES ('8', '产品运营支撑部', null, '1', null, null, null);
INSERT INTO `department` VALUES ('9', '创新孵化事业部', null, '1', null, null, null);
