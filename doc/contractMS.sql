﻿ -- MySQL dump 10.13  Distrib 5.7.14, for Win64 (x86_64)
--
-- Host: localhost    Database: contractms
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `adminID` varchar(100) NOT NULL COMMENT '管理员ID（时间戳+随机数4位）',
  `adminName` varchar(100) NOT NULL COMMENT '管理员账户名',
  `adminPass` varchar(200) NOT NULL COMMENT '管理员密码',
  `adminPrimarySalt` varchar(100) DEFAULT NULL COMMENT '管理员加密盐值（4位字母4位数字）',
  `adminStatus` smallint(5) unsigned DEFAULT '1' COMMENT '管理员状态（1可用，0禁用）',
  `adminCTime` datetime DEFAULT NULL COMMENT '管理员创建时间',
  `adminUTime` datetime DEFAULT NULL COMMENT '管理员更改时间',
  `adminDTime` datetime DEFAULT NULL COMMENT '管理员删除时间',
  PRIMARY KEY (`adminID`),
  UNIQUE KEY `admin_pk` (`adminName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contracts`
--

DROP TABLE IF EXISTS `contracts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contracts` (
  `contractID` varchar(100) NOT NULL COMMENT '合同ID（时间戳+随机数4位）',
  `contractSerial` varchar(100) NOT NULL COMMENT '合同编号',
  `contractName` varchar(200) NOT NULL COMMENT '合同名称',
  `userID` varchar(100) NOT NULL COMMENT '提交合同者ID',
  `deptID` varchar(100) NOT NULL COMMENT '提交者所属部门ID',
  `contractStatus` smallint(5) unsigned DEFAULT '1' COMMENT '合同状态（1可用，0禁用）',
  `contractCate` smallint(5) unsigned NOT NULL COMMENT '合同类别（1呈批件，2审批表，3合同）',
  `srStartTime` datetime DEFAULT NULL COMMENT '呈批件submit a report开始时间',
  `srEndTime` datetime DEFAULT NULL COMMENT '呈批件submit a report结束时间',
  `srRemindTime` datetime DEFAULT NULL COMMENT '呈批件submit a report提醒时间',
  `srIsWarn` smallint(5) unsigned DEFAULT NULL COMMENT '呈批件submit a report是否提醒（1提醒，0不提醒）',
  `eaStartTime` datetime DEFAULT NULL COMMENT '审批表examine and approve开始时间',
  `eaEndTime` datetime DEFAULT NULL COMMENT '审批表examine and approve结束时间',
  `eaRemindTime` datetime DEFAULT NULL COMMENT '审批表examine and approve提醒时间',
  `eaIsWarn` smallint(6) DEFAULT NULL COMMENT '审批表examine and approve是否提醒（1提醒，0不提醒）',
  `ctStartTime` datetime DEFAULT NULL COMMENT '合同Contract开始时间',
  `ctEndTime` datetime DEFAULT NULL COMMENT '合同Contract结束时间',
  `ctRemindTime` datetime DEFAULT NULL COMMENT '合同Contract提醒时间',
  `ctIsWarn` smallint(5) unsigned DEFAULT NULL COMMENT '合同Contract是否提醒（1提醒，0不提醒）',
  `ctPeriodNum` smallint(5) unsigned DEFAULT NULL COMMENT '合同分期付款期数',
  `contractCTime` datetime DEFAULT NULL COMMENT '合同（呈批件，审批表，合同）创建时间',
  `contractUTime` datetime DEFAULT NULL COMMENT '合同（呈批件，审批表，合同）更新时间',
  `contractDTime` datetime DEFAULT NULL COMMENT '合同（呈批件，审批表，合同）删除时间',
  PRIMARY KEY (`contractID`),
  UNIQUE KEY `contracts_pk` (`contractSerial`,`contractName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contracts`
--

LOCK TABLES `contracts` WRITE;
/*!40000 ALTER TABLE `contracts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contracts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `deptID` varchar(100) NOT NULL COMMENT '部门ID（时间戳+随机数4位）',
  `deptName` varchar(100) NOT NULL COMMENT '部门名称',
  `deptDes` varchar(500) DEFAULT NULL COMMENT '部门描述',
  `deptStatus` smallint(5) unsigned DEFAULT '1' COMMENT '部门状态（1可用，0禁用）',
  `deptCTime` datetime DEFAULT NULL COMMENT '部门创建时间',
  `deptUTime` datetime DEFAULT NULL COMMENT '部门更新时间',
  `deptDTime` datetime DEFAULT NULL COMMENT '部门删除时间',
  PRIMARY KEY (`deptID`),
  UNIQUE KEY `department_pk` (`deptName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `logID` varchar(100) NOT NULL COMMENT '日志ID（时间戳+随机数4位）',
  `userID` varchar(100) DEFAULT NULL COMMENT '用户ID（包括管理员和用户）',
  `userIPforLog` varchar(100) DEFAULT NULL COMMENT '产生日志时对应用户的IP',
  `logOperRole` smallint(5) unsigned DEFAULT NULL COMMENT '产生日志时对应用户的角色（1总经理，2部门经理，3普通用户，0管理员）',
  `logOper` text COMMENT '操作/日志内容',
  `logStatus` smallint(5) unsigned DEFAULT '1' COMMENT '日志状态（1可用，0禁用）',
  `logCTime` datetime DEFAULT NULL COMMENT '日志创建时间（用户操作时间）',
  `logDTime` datetime DEFAULT NULL COMMENT '日志删除时间',
  PRIMARY KEY (`logID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `period`
--

DROP TABLE IF EXISTS `period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `period` (
  `periodID` varchar(100) NOT NULL COMMENT '分期ID（时间戳+随机数4位）',
  `contractID` varchar(100) NOT NULL COMMENT '分期对应合同ID',
  `periodRemindTime` datetime DEFAULT NULL COMMENT '提醒时间',
  `periodPaidTime` datetime DEFAULT NULL COMMENT '付款时间',
  `periodIsWarn` smallint(5) unsigned DEFAULT '1' COMMENT '是否提醒（1提醒，0不提醒）',
  `periodMoney` double DEFAULT '0' COMMENT '付款金额',
  `periodStatus` smallint(5) unsigned DEFAULT '1' COMMENT '分期状态（1可用，0禁用）',
  `periodCTime` datetime DEFAULT NULL COMMENT '分期创建时间',
  `periodUTime` datetime DEFAULT NULL COMMENT '分期更新时间',
  `periodDTime` datetime DEFAULT NULL COMMENT '分期删除时间',
  PRIMARY KEY (`periodID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `period`
--

LOCK TABLES `period` WRITE;
/*!40000 ALTER TABLE `period` DISABLE KEYS */;
/*!40000 ALTER TABLE `period` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userID` varchar(100) NOT NULL COMMENT '用户ID（时间戳+随机数4位）',
  `userName` varchar(100) NOT NULL COMMENT '用户名称（员工编号）',
  `userPass` varchar(200) NOT NULL COMMENT '用户密码',
  `deptID` varchar(100) NOT NULL COMMENT '用户部门ID',
  `userOAEmail` varchar(100) DEFAULT NULL COMMENT '用户OA邮箱',
  `userOrdinaryEmail` varchar(100) DEFAULT NULL COMMENT '用户普通邮箱',
  `userFullName` varchar(100) DEFAULT NULL COMMENT '用户姓名',
  `userPhone` varchar(100) DEFAULT NULL COMMENT '用户手机',
  `userRight` smallint(5) unsigned DEFAULT '3' COMMENT '用户权限（1总经理，2部门经理，3普通员工）',
  `userPrimarySalt` varchar(100) DEFAULT NULL COMMENT '用户加密盐值（4位字母4位数字）',
  `userStatus` smallint(5) unsigned DEFAULT '1' COMMENT '用户状态（1可用，0禁用）',
  `userCTime` datetime DEFAULT NULL COMMENT '用户创建时间',
  `userUTime` datetime DEFAULT NULL COMMENT '用户信息更新时间',
  `userDTime` datetime DEFAULT NULL COMMENT '用户删除时间',
  PRIMARY KEY (`userID`),
  UNIQUE KEY `user_pk` (`userName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-18 22:15:08
-- ----------------------------
-- View structure for alluser
-- ----------------------------
DROP VIEW IF EXISTS `alluser`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` 
SQL SECURITY DEFINER VIEW `alluser` AS 
select `u`.`userID` AS `userID`,`u`.`userName` AS `userName` 
from `user` `u` 
union 
select `a`.`adminID` AS `userID`,`a`.`adminID` AS `userName` 
from `admin` `a` ;