/*
Navicat MySQL Data Transfer

Source Server         : 183.224.41.189
Source Server Version : 50717
Source Host           : 183.224.41.189:8087
Source Database       : ContractMS

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-01-09 09:49:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `adminID` varchar(100) NOT NULL COMMENT '管理员ID（时间戳+随机数4位）',
  `adminName` varchar(100) NOT NULL COMMENT '管理员账户名',
  `adminPass` varchar(200) NOT NULL COMMENT '管理员密码',
  `adminPrimarySalt` varchar(100) DEFAULT NULL COMMENT '管理员加密盐值（4位字母4位数字）',
  `adminStatus` smallint(5) unsigned DEFAULT '1' COMMENT '管理员状态（1可用，0禁用）',
  `adminCTime` datetime DEFAULT NULL COMMENT '管理员创建时间',
  `adminUTime` datetime DEFAULT NULL COMMENT '管理员更改时间',
  `adminDTime` datetime DEFAULT NULL COMMENT '管理员删除时间',
  PRIMARY KEY (`adminID`),
  UNIQUE KEY `admin_pk` (`adminName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', 'c7122a1349c22cb3c009da3613d242ab', 'admin', '1', null, null, null);
