package com.yncmcc.Mail;

import com.yncmcc.Entity.ContractsEntity;
import com.yncmcc.Entity.PeriodEntity;
import com.yncmcc.Entity.UserEntity;
import com.yncmcc.Exception.MailAddressException;
import com.yncmcc.Service.Interface.ContractsService;
import com.yncmcc.Service.Interface.LogsService;
import com.yncmcc.Service.Interface.PeriodService;
import com.yncmcc.Service.Interface.UserService;
import com.yncmcc.Util.JavaMail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by CMCC-sunmo on 2016/9/18.
 */
@Component
public class PeriodMail {
    private static Short AdminRight = 0;
    @Autowired
    private PeriodService periodService;
    @Autowired
    private UserService userServie;
    @Autowired
    private ContractsService contractsService;
    private JavaMail javaMail;
    private String host;
    private String senderAddr;
    private String userName;
    private String password;

    @Autowired
    private LogsService Log;

    public PeriodMail() {
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setSenderAddr(String senderAddr) {
        this.senderAddr = senderAddr;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void run() {
        PeriodEntity[] periods = periodService.findByRemindDate();
        if (periods.length > 0) {
            javaMail = new JavaMail();
            javaMail.initSender(host, senderAddr, userName, password);
            for (PeriodEntity period : periods) {
                ContractsEntity contractEntity = contractsService.findByContractID(period.getContractId());
                UserEntity userEntity = userServie.findByUserId(contractEntity.getUserId());
                String content = "尊敬的" + userEntity.getUserFullName() + "先生/女士您好：\r\n" +
                        "    您的合同 " + contractEntity.getContractName() + " （合同号：" + contractEntity.getContractSerial() + "） 即将"
                        + "交付下一次款项，请您注意，谢谢。如果不想继续接收此合同的分期付款提醒，请到合同管理系统中取消提醒！";

                if(userEntity.getUserOaEmail() != null || userEntity.getUserOaEmail() != "")
                {
                    javaMail.initReceiver(userEntity.getUserOaEmail(), "付款提醒", content, false, "2000");

                    try {
                        javaMail.send();
                        Log.saveLog("system","183.224.41.189",AdminRight, "发送统一邮件：{userOaMail:"+userEntity.getUserOaEmail()+" ,合同号：" + contractEntity.getContractSerial() +"}");
                    } catch (MailAddressException e) {
                        e.printStackTrace();
                    }
                }
                if(userEntity.getUserOrdinaryEmail()!=null || userEntity.getUserOrdinaryEmail()!="") {
                    javaMail.initReceiver(userEntity.getUserOrdinaryEmail(), "付款提醒", content, false, "2000");

                    try {
                        javaMail.send();
                        Log.saveLog("system","183.224.41.189",AdminRight, "发送139邮件：{userOrdinaryMail:"+userEntity.getUserOrdinaryEmail()+" ,合同号：" + contractEntity.getContractSerial() +"}");
                    } catch (MailAddressException e) {
                        e.printStackTrace();
                    }
                }
//                System.out.println("Send 付款 Mail");
            }
        }
    }
}
