package com.yncmcc.Mail;

import com.yncmcc.Entity.ContractsCustomEntity;
import com.yncmcc.Entity.ContractsEntity;
import com.yncmcc.Entity.UserEntity;
import com.yncmcc.Exception.MailAddressException;
import com.yncmcc.Service.Interface.ContractsService;
import com.yncmcc.Service.Interface.LogsService;
import com.yncmcc.Service.Interface.UserService;
import com.yncmcc.Util.JavaMail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by CMCC-sunmo on 2016/9/18.
 */
@Component
public class ContractMail {
    private static Short AdminRight = 0;
    @Autowired
    private ContractsService contractService;
    @Autowired
    private UserService userServie;
    private JavaMail javaMail;
    private String host;
    private String senderAddr;
    private String userName;
    private String password;

    @Autowired
    private LogsService Log;

    public ContractMail() {
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setSenderAddr(String senderAddr) {
        this.senderAddr = senderAddr;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void run() {
        sendSrContract();
        sendEaContract();
        sendCtContract();

    }

    private void sendSrContract(){
        List<ContractsCustomEntity> srContractList = contractService.findBySrRemindTime();
        if(srContractList.size()>0){
            javaMail = new JavaMail();
            javaMail.initSender(host, senderAddr, userName, password);
            for (ContractsCustomEntity contract : srContractList) {
                UserEntity userEntity = userServie.findByUserId(contract.getUserId());
                String content = "尊敬的" + userEntity.getUserFullName() + "先生/女士您好：\r\n" +
                        "    您的呈批件 " + contract.getContractName() + " （合同号：" + contract.getContractSerial() + "） 即将于" + contract.getSrEndTime()
                        + "到期，请您注意，谢谢。如果不想继续接收此呈批件的到期提醒，请到合同管理系统中取消提醒！";
                if(userEntity.getUserOaEmail() != null || userEntity.getUserOaEmail() != "")
                {
                    javaMail.initReceiver(userEntity.getUserOaEmail(), "呈批件到期提醒", content, false, "2000");

                    try {
                        javaMail.send();
                        Log.saveLog("system","183.224.41.189",AdminRight, "发送统一邮件：{userOAMail:"+userEntity.getUserOaEmail()+" ,合同号：" + contract.getContractSerial() +"}");
                    } catch (MailAddressException e) {
                        e.printStackTrace();
                    }
                }
                if(userEntity.getUserOrdinaryEmail()!=null || userEntity.getUserOrdinaryEmail()!="") {
                    javaMail.initReceiver(userEntity.getUserOrdinaryEmail(), "呈批件到期提醒", content, false, "2000");

                    try {
                        javaMail.send();
                        Log.saveLog("system","183.224.41.189",AdminRight, "发送139邮件：{userOrdinaryMail:"+userEntity.getUserOrdinaryEmail()+" ,合同号：" + contract.getContractSerial() +"}");
                    } catch (MailAddressException e) {
                        e.printStackTrace();
                    }
                }
//                System.out.println("Send 呈批件 Mail");
            }
        }
    }
    private void sendEaContract(){
        List<ContractsCustomEntity> eaContractList = contractService.findByEaRemindTime();
        if(eaContractList.size()>0){
            javaMail = new JavaMail();
            javaMail.initSender(host, senderAddr, userName, password);
            for (ContractsCustomEntity contract : eaContractList) {
                UserEntity userEntity = userServie.findByUserId(contract.getUserId());
                String content = "尊敬的" + userEntity.getUserFullName() + "先生/女士您好：\r\n" +
                        "    您的审批表 " + contract.getContractName() + " （合同号：" + contract.getContractSerial() + "） 即将于" + contract.getEaEndTime()
                        + "到期，请您注意，谢谢。如果不想继续接收此审批表的到期提醒，请到合同管理系统中取消提醒！";
                if(userEntity.getUserOaEmail() != null || userEntity.getUserOaEmail() != "")
                {
                    javaMail.initReceiver(userEntity.getUserOaEmail(), "审批表到期提醒", content, false, "2000");

                    try {
                        javaMail.send();
                        Log.saveLog("system","183.224.41.189",AdminRight, "发送统一邮件：{userOaMail:"+userEntity.getUserOaEmail()+" ,合同号：" + contract.getContractSerial() +"}");
                    } catch (MailAddressException e) {
                        e.printStackTrace();
                    }
                }
                if(userEntity.getUserOrdinaryEmail()!=null || userEntity.getUserOrdinaryEmail()!="") {
                    javaMail.initReceiver(userEntity.getUserOrdinaryEmail(), "审批表到期提醒", content, false, "2000");

                    try {
                        javaMail.send();
                        Log.saveLog("system","183.224.41.189",AdminRight, "发送139邮件：{userOrdinaryMail:"+userEntity.getUserOrdinaryEmail()+" ,合同号：" + contract.getContractSerial() +"}");
                    } catch (MailAddressException e) {
                        e.printStackTrace();
                    }
                }
//                System.out.println("Send 审批表 Mail");
            }
        }
    }
    private void sendCtContract(){
        List<ContractsCustomEntity> ctContractList = contractService.findByCtRemindTime();
        if(ctContractList.size()>0){
            javaMail = new JavaMail();
            javaMail.initSender(host, senderAddr, userName, password);
            for (ContractsCustomEntity contract : ctContractList) {
                UserEntity userEntity = userServie.findByUserId(contract.getUserId());
                String content = "尊敬的" + userEntity.getUserFullName() + "先生/女士您好：\r\n" +
                        "    您的合同 " + contract.getContractName() + " （合同号：" + contract.getContractSerial() + "） 即将于" + contract.getCtEndTime()
                        + "到期，请您注意，谢谢。如果不想继续接收此合同的到期提醒，请到合同管理系统中取消提醒！";
                if(userEntity.getUserOaEmail() != null || userEntity.getUserOaEmail() != "")
                {
                    javaMail.initReceiver(userEntity.getUserOaEmail(), "合同到期提醒", content, false, "2000");

                    try {
                        javaMail.send();
                        Log.saveLog("system","183.224.41.189",AdminRight, "发送统一邮件：{userOaMail:"+userEntity.getUserOaEmail()+" ,合同号：" + contract.getContractSerial() +"}");
                    } catch (MailAddressException e) {
                        e.printStackTrace();
                    }
                }
                if(userEntity.getUserOrdinaryEmail()!=null || userEntity.getUserOrdinaryEmail()!="") {
                    javaMail.initReceiver(userEntity.getUserOrdinaryEmail(), "合同到期提醒", content, false, "2000");

                    try {
                        javaMail.send();
                        Log.saveLog("system","183.224.41.189",AdminRight, "发送139邮件：{userOrdinaryMail:"+userEntity.getUserOrdinaryEmail()+" ,合同号：" + contract.getContractSerial() +"}");
                    } catch (MailAddressException e) {
                        e.printStackTrace();
                    }
                }
//                System.out.println("Send 合同 Mail");
            }
        }
    }
}
