package com.yncmcc.Repository.Interface;

import com.yncmcc.Entity.ContractsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by CMCC-sunmo on 2016/9/18.
 */
@Repository
public interface ContractsRepository extends JpaRepository<ContractsEntity, String>,ContractsRepositoryCustom {

    @Query("from ContractsEntity c where c.contractId = ? and c.contractStatus = 1")
    ContractsEntity findByContractID(String ContractId);

    @Query("from ContractsEntity c where c.contractSerial = ? and c.contractStatus = 1")
    ContractsEntity findByContractSerial(String contractSerial);



}
