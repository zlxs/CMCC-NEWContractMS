package com.yncmcc.Repository.Interface;

import com.yncmcc.Entity.DepartmentEntity;
import com.yncmcc.Entity.UserEntity;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Arabira on 2016/8/3.
 */
@Repository
public interface  UserRepository extends JpaRepository<UserEntity, String>,UserRepositoryCustom {

    @Query("from UserEntity u where u.userId = ? and u.userStatus = 1")
    UserEntity findByUserId(String userId);

    @Query("from UserEntity u where u.userName = ? and u.userStatus = 1")
    UserEntity findByUserName(String userName);

}
