package com.yncmcc.Repository.Interface;

import com.yncmcc.Entity.LogsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;

/**
 * Created by CMCC-sunmo on 2016/8/3.
 */
@Repository
public interface LogsRepository extends JpaRepository<LogsEntity, Integer>,LogsRepositoryCustom {

    @Query("from LogsEntity l where l.logId = ?")
    LogsEntity findByLogID(String logId);

}
