package com.yncmcc.Repository.Interface;

import com.yncmcc.Entity.ContractsCustomEntity;
import com.yncmcc.Entity.ContractsEntity;

import java.util.List;

/**
 * Created by CMCC-Sunmo on 2016/9/20.
 */
public interface ContractsRepositoryCustom {
    List<ContractsCustomEntity> findReqContract(ContractsEntity contractsEntity);
    List<ContractsCustomEntity> findBySrRemindTime();
    List<ContractsCustomEntity> findByEaRemindTime();
    List<ContractsCustomEntity> findByCtRemindTime();
}
