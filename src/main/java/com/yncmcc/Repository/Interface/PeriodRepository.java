package com.yncmcc.Repository.Interface;

import com.yncmcc.Entity.PeriodEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by CMCC-sunmo on 2016/9/18.
 */
public interface PeriodRepository extends JpaRepository<PeriodEntity, Integer> {

    @Query("from PeriodEntity p where p.contractId = ? and p.periodStatus = 1")
    List<PeriodEntity> findByContractID(String ContractId);

  
    @Query("from PeriodEntity p where p.periodRemindTime between p.periodRemindTime and ? and p.periodPaidTime between ? and p.periodPaidTime and p.periodIsWarn = 1")
    PeriodEntity[] findByRemindDate(Timestamp after,Timestamp after1);

    @Query("from PeriodEntity p where p.periodId = ? and p.periodStatus = 1")
    PeriodEntity findByperiodId(String periodId);
}
