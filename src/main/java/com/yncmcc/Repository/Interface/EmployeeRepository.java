package com.yncmcc.Repository.Interface;

import com.yncmcc.Entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by LiJiQiu on 2017/7/24.
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, String> {
    @Query("from Employee e where e.fullName = ?")
    Employee getEmployeeByFullName(String fullName);
}
