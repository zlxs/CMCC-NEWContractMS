package com.yncmcc.Repository.Interface;

import com.yncmcc.Entity.AdminEntity;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by CMCC-sunmo on 2016/8/26.
 */
@Repository
public interface AdminRepository extends JpaRepository<AdminEntity, String> {
    @Query("from AdminEntity a where a.adminName = ? and a.adminStatus = 1")
    AdminEntity findByAdminName(String adminName);

}
