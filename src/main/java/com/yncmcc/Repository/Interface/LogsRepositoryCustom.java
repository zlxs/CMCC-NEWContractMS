package com.yncmcc.Repository.Interface;

import com.yncmcc.Entity.LogsCustomEntity;
import com.yncmcc.Entity.LogsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by CMCC-sunmo on 2016/8/3.
 */
public interface LogsRepositoryCustom {

    List<LogsCustomEntity> findLogs(LogsEntity logsEntity);

}
