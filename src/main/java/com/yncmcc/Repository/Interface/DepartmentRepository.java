package com.yncmcc.Repository.Interface;

import com.yncmcc.Entity.AdminEntity;
import com.yncmcc.Entity.ContractsEntity;
import com.yncmcc.Entity.DepartmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by CMCC-sunmo on 2016/8/26.
 */
@Repository
public interface DepartmentRepository extends JpaRepository<DepartmentEntity, String> {

    @Query("from DepartmentEntity d where d.deptStatus = 1 order by d.deptDes asc ")
    List<DepartmentEntity> findDepartmentAll();

    @Query("from DepartmentEntity d where d.deptName  = ? and d.deptStatus = 1")
    DepartmentEntity findByDepartmentName(String departmentName);
}
