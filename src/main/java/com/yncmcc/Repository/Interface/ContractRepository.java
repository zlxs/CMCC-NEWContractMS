package com.yncmcc.Repository.Interface;

import com.yncmcc.Entity.Contract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by LiJiQiu on 2017/7/24.
 */
@Repository
public interface ContractRepository extends JpaRepository<Contract, String> {
    @Query("from Contract c where c.stage <> 6")
    List<Contract> getActiveContract();
}
