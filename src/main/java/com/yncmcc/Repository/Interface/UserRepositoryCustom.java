package com.yncmcc.Repository.Interface;

import com.yncmcc.Entity.UserEntity;

import java.util.Collection;
import java.util.List;

/**
 * Created by CMCC-Sunmo on 2016/9/21.
 */
public interface UserRepositoryCustom {
    List<UserEntity> findUser(UserEntity userEntity);
}
