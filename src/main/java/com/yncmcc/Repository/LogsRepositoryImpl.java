package com.yncmcc.Repository;

import com.yncmcc.Entity.LogsCustomEntity;
import com.yncmcc.Entity.LogsEntity;
import com.yncmcc.Repository.Interface.LogsRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by CMCC-Sunmo on 2016/9/21.
 */
public class LogsRepositoryImpl implements LogsRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<LogsCustomEntity> findLogs(LogsEntity logsEntity) {

        List<Short> right = new ArrayList<Short>();
        Timestamp logCTimeBegin = null;
        Timestamp logCTimeEND = null;
        if(logsEntity.getLogOperRole() == null)
        {
            right.add((Short) (short) 0);
            right.add((Short) (short) 1);
            right.add((Short) (short) 2);
            right.add((Short) (short) 3);
        }else {
            if (logsEntity.getLogOperRole() == 0) {
                right.add((Short) (short) 0);
            } else if (logsEntity.getLogOperRole() == 1) {
                right.add((Short) (short) 1);
            } else if (logsEntity.getLogOperRole() == 2) {
                right.add((Short) (short) 2);
            } else if (logsEntity.getLogOperRole() == 3) {
                right.add((Short) (short) 3);
            }
        }


        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if(logsEntity.getLogCTime() == null || logsEntity.getLogCTime().equals(Timestamp.valueOf("2000-01-01 00:00:00"))){
            logCTimeBegin = Timestamp.valueOf("2000-01-01 00:00:01");
            logCTimeEND = Timestamp.valueOf("9999-12-31 23:59:59");
        }else{
            String logCTime = sdf.format(logsEntity.getLogCTime());
            logCTimeBegin = Timestamp.valueOf(logCTime +" 00:00:01");
            logCTimeEND = Timestamp.valueOf(logCTime +" 23:59:59");
        }

        List<LogsCustomEntity> logsCustomEntities = entityManager.createNativeQuery(
//                "SELECT l.*,u.userName,d.deptName " +
//                "FROM logs l,user u, department d " +
//                "WHERE l.userID=u.userID AND u.deptID=d.deptID " +
//                "AND l.logStatus = 1 " +
//                "AND u.userName like :userName " +
//                "AND l.userIPforLog like :userIPforLog " +
//                "AND l.logOperRole IN :logOperRole " +
//                "AND l.logOper like :logOper " +
//                "AND l.logCTime BETWEEN :logCTimeBegin AND :logCTimeEND ",LogsCustomEntity.class)
//                .setParameter("userName",logsEntity.getUserId()==null?"%%":"%"+logsEntity.getUserId()+"%")
//                .setParameter("userIPforLog",logsEntity.getUserIPforLog()==null?"%%":"%"+logsEntity.getUserIPforLog()+"%")
//                .setParameter("logOperRole",right)
//                .setParameter("logOper",logsEntity.getLogOper()==null?"%%":"%"+logsEntity.getLogOper()+"%")
//                .setParameter("logCTimeBegin",(Date) logCTimeBegin)
//                .setParameter("logCTimeEND",(Date) logCTimeEND)
//                .getResultList();
                "SELECT l.*,u.userName " +
                "FROM logs l,alluser u " +
                "WHERE l.userID=u.userID " +
                "AND l.logStatus = 1 " +
                "AND u.userName like :userName " +
                "AND l.userIPforLog like :userIPforLog " +
                "AND l.logOperRole IN :logOperRole " +
                "AND l.logOper like :logOper " +
                "AND l.logCTime BETWEEN :logCTimeBegin AND :logCTimeEND ",LogsCustomEntity.class)
                .setParameter("userName",logsEntity.getUserId()==null?"%%":"%"+logsEntity.getUserId()+"%")
                .setParameter("userIPforLog",logsEntity.getUserIPforLog()==null?"%%":"%"+logsEntity.getUserIPforLog()+"%")
                .setParameter("logOperRole",right)
                .setParameter("logOper",logsEntity.getLogOper()==null?"%%":"%"+logsEntity.getLogOper()+"%")
                .setParameter("logCTimeBegin",(Date) logCTimeBegin)
                .setParameter("logCTimeEND",(Date) logCTimeEND)
                .getResultList();

        return logsCustomEntities;
    }
}
