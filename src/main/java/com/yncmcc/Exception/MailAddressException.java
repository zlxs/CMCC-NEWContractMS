package com.yncmcc.Exception;

/**
 * Created by Arabira on 2016/8/10.
 */
public class MailAddressException extends Exception {

    private static final long serialVersionUID = -1967042567087469542L;

    public MailAddressException() {
    }

    public MailAddressException(String message) {
        super(message);
    }

    public MailAddressException(String message, Throwable cause) {
        super(message, cause);
    }

    public MailAddressException(Throwable cause) {
        super(cause);
    }

    public MailAddressException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
