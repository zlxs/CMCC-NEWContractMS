package com.yncmcc.Entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import com.yncmcc.Entity.ContractsEntity;

import java.sql.Timestamp;

/**
 * Created by CMCC-Sunmo on 2016/9/20.
 */
@Generated("org.hibernate.jpamodelgen.JPAMetaModelEntityProcesso")
@StaticMetamodel(ContractsEntity.class)
public class ContractsEntity_ {
    public static volatile SingularAttribute<ContractsEntity, String> contractId;

    public static volatile SingularAttribute<ContractsEntity, String> contractSerial;

    public static volatile SingularAttribute<ContractsEntity, String> contractName;
    public static volatile SingularAttribute<ContractsEntity, String> userId;
    public static volatile SingularAttribute<ContractsEntity, String> deptId;
    public static volatile SingularAttribute<ContractsEntity, Short> contractStatus;
    public static volatile SingularAttribute<ContractsEntity, Short> contractCate;
    public static volatile SingularAttribute<ContractsEntity, Timestamp> srStartTime;
    public static volatile SingularAttribute<ContractsEntity, Timestamp> srEndTime;
    public static volatile SingularAttribute<ContractsEntity, Timestamp> srRemindTime;
    public static volatile SingularAttribute<ContractsEntity, Short> srIsWarn;
    public static volatile SingularAttribute<ContractsEntity, Timestamp> eaStartTime;
    public static volatile SingularAttribute<ContractsEntity, Timestamp> eaEndTime;
    public static volatile SingularAttribute<ContractsEntity, Timestamp> eaRemindTime;
    public static volatile SingularAttribute<ContractsEntity, Short> eaIsWarn;
    public static volatile SingularAttribute<ContractsEntity, Timestamp> ctStartTime;
    public static volatile SingularAttribute<ContractsEntity, Timestamp> ctEndTime;
    public static volatile SingularAttribute<ContractsEntity, Timestamp> ctRemindTime;
    public static volatile SingularAttribute<ContractsEntity, Short> ctIsWarn;

    public static volatile SingularAttribute<ContractsEntity, Short> ctPeriodNum;
    public static volatile SingularAttribute<ContractsEntity, Timestamp> contractCTime;
    public static volatile SingularAttribute<ContractsEntity, Timestamp> contractUTime;
    public static volatile SingularAttribute<ContractsEntity, Timestamp> contractDTime;

    public static volatile ListAttribute<ContractsEntity, UserEntity> userEntity;

}
