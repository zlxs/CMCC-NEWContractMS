package com.yncmcc.Entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by CMCC-Sunmo on 2016/9/15.
 */
@Entity
@Table(name = "contracts", schema = "contractms", catalog = "")
public class ContractsEntity {
    private String contractId;
    private String contractSerial;
    private String contractName;
    private String userId;
    private String deptId;
    private Short contractStatus;
    private Short contractCate;
    private Timestamp srStartTime;
    private Timestamp srEndTime;
    private Timestamp srRemindTime;
    private Short srIsWarn;
    private Timestamp eaStartTime;
    private Timestamp eaEndTime;
    private Timestamp eaRemindTime;
    private Short eaIsWarn;
    private Timestamp ctStartTime;
    private Timestamp ctEndTime;
    private Timestamp ctRemindTime;
    private Short ctIsWarn;
    private Short ctPeriodNum;
    private Timestamp contractCTime;
    private Timestamp contractUTime;
    private Timestamp contractDTime;

    @ManyToOne
    @JoinColumn(name = "userId", referencedColumnName = "userId")
    private UserEntity userEntity;

    @Id
    @Column(name = "contractID")
    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    @Basic
    @Column(name = "contractSerial")
    public String getContractSerial() {
        return contractSerial;
    }

    public void setContractSerial(String contractSerial) {
        this.contractSerial = contractSerial;
    }

    @Basic
    @Column(name = "contractName")
    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    @Basic
    @Column(name = "userID")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "deptID")
    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    @Basic
    @Column(name = "contractStatus")
    public Short getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(Short contractStatus) {
        this.contractStatus = contractStatus;
    }

    @Basic
    @Column(name = "contractCate")
    public Short getContractCate() {
        return contractCate;
    }

    public void setContractCate(Short contractCate) {
        this.contractCate = contractCate;
    }

    @Basic
    @Column(name = "srStartTime")
    public Timestamp getSrStartTime() {
        return srStartTime;
    }

    public void setSrStartTime(Timestamp srStartTime) {
        this.srStartTime = srStartTime;
    }

    @Basic
    @Column(name = "srEndTime")
    public Timestamp getSrEndTime() {
        return srEndTime;
    }

    public void setSrEndTime(Timestamp srEndTime) {
        this.srEndTime = srEndTime;
    }

    @Basic
    @Column(name = "srRemindTime")
    public Timestamp getSrRemindTime() {
        return srRemindTime;
    }

    public void setSrRemindTime(Timestamp srRemindTime) {
        this.srRemindTime = srRemindTime;
    }

    @Basic
    @Column(name = "srIsWarn")
    public Short getSrIsWarn() {
        return srIsWarn;
    }

    public void setSrIsWarn(Short srIsWarn) {
        this.srIsWarn = srIsWarn;
    }

    @Basic
    @Column(name = "eaStartTime")
    public Timestamp getEaStartTime() {
        return eaStartTime;
    }

    public void setEaStartTime(Timestamp eaStartTime) {
        this.eaStartTime = eaStartTime;
    }

    @Basic
    @Column(name = "eaEndTime")
    public Timestamp getEaEndTime() {
        return eaEndTime;
    }

    public void setEaEndTime(Timestamp eaEndTime) {
        this.eaEndTime = eaEndTime;
    }

    @Basic
    @Column(name = "eaRemindTime")
    public Timestamp getEaRemindTime() {
        return eaRemindTime;
    }

    public void setEaRemindTime(Timestamp eaRemindTime) {
        this.eaRemindTime = eaRemindTime;
    }

    @Basic
    @Column(name = "eaIsWarn")
    public Short getEaIsWarn() {
        return eaIsWarn;
    }

    public void setEaIsWarn(Short eaIsWarn) {
        this.eaIsWarn = eaIsWarn;
    }

    @Basic
    @Column(name = "ctStartTime")
    public Timestamp getCtStartTime() {
        return ctStartTime;
    }

    public void setCtStartTime(Timestamp ctStartTime) {
        this.ctStartTime = ctStartTime;
    }

    @Basic
    @Column(name = "ctEndTime")
    public Timestamp getCtEndTime() {
        return ctEndTime;
    }

    public void setCtEndTime(Timestamp ctEndTime) {
        this.ctEndTime = ctEndTime;
    }

    @Basic
    @Column(name = "ctRemindTime")
    public Timestamp getCtRemindTime() {
        return ctRemindTime;
    }

    public void setCtRemindTime(Timestamp ctRemindTime) {
        this.ctRemindTime = ctRemindTime;
    }

    @Basic
    @Column(name = "ctIsWarn")
    public Short getCtIsWarn() {
        return ctIsWarn;
    }

    public void setCtIsWarn(Short ctIsWarn) {
        this.ctIsWarn = ctIsWarn;
    }

    @Basic
    @Column(name = "ctPeriodNum")
    public Short getCtPeriodNum() {
        return ctPeriodNum;
    }

    public void setCtPeriodNum(Short ctPeriodNum) {
        this.ctPeriodNum = ctPeriodNum;
    }

    @Basic
    @Column(name = "contractCTime")
    public Timestamp getContractCTime() {
        return contractCTime;
    }

    public void setContractCTime(Timestamp contractCTime) {
        this.contractCTime = contractCTime;
    }

    @Basic
    @Column(name = "contractUTime")
    public Timestamp getContractUTime() {
        return contractUTime;
    }

    public void setContractUTime(Timestamp contractUTime) {
        this.contractUTime = contractUTime;
    }

    @Basic
    @Column(name = "contractDTime")
    public Timestamp getContractDTime() {
        return contractDTime;
    }

    public void setContractDTime(Timestamp contractDTime) {
        this.contractDTime = contractDTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContractsEntity that = (ContractsEntity) o;

        if (contractId != null ? !contractId.equals(that.contractId) : that.contractId != null) return false;
        if (contractSerial != null ? !contractSerial.equals(that.contractSerial) : that.contractSerial != null)
            return false;
        if (contractName != null ? !contractName.equals(that.contractName) : that.contractName != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (deptId != null ? !deptId.equals(that.deptId) : that.deptId != null) return false;
        if (contractStatus != null ? !contractStatus.equals(that.contractStatus) : that.contractStatus != null)
            return false;
        if (contractCate != null ? !contractCate.equals(that.contractCate) : that.contractCate != null)
            return false;
        if (srStartTime != null ? !srStartTime.equals(that.srStartTime) : that.srStartTime != null) return false;
        if (srEndTime != null ? !srEndTime.equals(that.srEndTime) : that.srEndTime != null) return false;
        if (srRemindTime != null ? !srRemindTime.equals(that.srRemindTime) : that.srRemindTime != null) return false;
        if (srIsWarn != null ? !srIsWarn.equals(that.srIsWarn) : that.srIsWarn != null) return false;
        if (eaStartTime != null ? !eaStartTime.equals(that.eaStartTime) : that.eaStartTime != null) return false;
        if (eaEndTime != null ? !eaEndTime.equals(that.eaEndTime) : that.eaEndTime != null) return false;
        if (eaRemindTime != null ? !eaRemindTime.equals(that.eaRemindTime) : that.eaRemindTime != null) return false;
        if (eaIsWarn != null ? !eaIsWarn.equals(that.eaIsWarn) : that.eaIsWarn != null) return false;
        if (ctStartTime != null ? !ctStartTime.equals(that.ctStartTime) : that.ctStartTime != null) return false;
        if (ctEndTime != null ? !ctEndTime.equals(that.ctEndTime) : that.ctEndTime != null) return false;
        if (ctRemindTime != null ? !ctRemindTime.equals(that.ctRemindTime) : that.ctRemindTime != null) return false;
        if (ctIsWarn != null ? !ctIsWarn.equals(that.ctIsWarn) : that.ctIsWarn != null) return false;
        if (ctPeriodNum != null ? !ctPeriodNum.equals(that.ctPeriodNum) : that.ctPeriodNum != null) return false;
        if (contractCTime != null ? !contractCTime.equals(that.contractCTime) : that.contractCTime != null)
            return false;
        if (contractUTime != null ? !contractUTime.equals(that.contractUTime) : that.contractUTime != null)
            return false;
        if (contractDTime != null ? !contractDTime.equals(that.contractDTime) : that.contractDTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = contractId != null ? contractId.hashCode() : 0;
        result = 31 * result + (contractSerial != null ? contractSerial.hashCode() : 0);
        result = 31 * result + (contractName != null ? contractName.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (deptId != null ? deptId.hashCode() : 0);
        result = 31 * result + (contractStatus != null ? contractStatus.hashCode() : 0);
        result = 31 * result + (contractCate != null ? contractCate.hashCode() : 0);
        result = 31 * result + (srStartTime != null ? srStartTime.hashCode() : 0);
        result = 31 * result + (srEndTime != null ? srEndTime.hashCode() : 0);
        result = 31 * result + (srRemindTime != null ? srRemindTime.hashCode() : 0);
        result = 31 * result + (srIsWarn != null ? srIsWarn.hashCode() : 0);
        result = 31 * result + (eaStartTime != null ? eaStartTime.hashCode() : 0);
        result = 31 * result + (eaEndTime != null ? eaEndTime.hashCode() : 0);
        result = 31 * result + (eaRemindTime != null ? eaRemindTime.hashCode() : 0);
        result = 31 * result + (eaIsWarn != null ? eaIsWarn.hashCode() : 0);
        result = 31 * result + (ctStartTime != null ? ctStartTime.hashCode() : 0);
        result = 31 * result + (ctEndTime != null ? ctEndTime.hashCode() : 0);
        result = 31 * result + (ctRemindTime != null ? ctRemindTime.hashCode() : 0);
        result = 31 * result + (ctIsWarn != null ? ctIsWarn.hashCode() : 0);
        result = 31 * result + (ctPeriodNum != null ? ctPeriodNum.hashCode() : 0);
        result = 31 * result + (contractCTime != null ? contractCTime.hashCode() : 0);
        result = 31 * result + (contractUTime != null ? contractUTime.hashCode() : 0);
        result = 31 * result + (contractDTime != null ? contractDTime.hashCode() : 0);
        return result;
    }
}
