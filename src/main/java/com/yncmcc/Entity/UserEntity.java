package com.yncmcc.Entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by CMCC-Sunmo on 2016/9/15.
 */
@Entity
@Table(name = "user", schema = "contractms", catalog = "")
public class UserEntity {
    private String userId;
    private String userName;
    private String userPass;
    private String deptId;
    private String userOaEmail;
    private String userOrdinaryEmail;
    private String userFullName;
    private String userPhone;
    private Short userRight;
    private String userPrimarySalt;
    private Short userStatus;
    private Timestamp userCTime;
    private Timestamp userUTime;
    private Timestamp userDTime;

    @OneToMany
    private List<ContractsEntity> contractsEntityList;

    @Id
    @Column(name = "userID")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "userName")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "userPass")
    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    @Basic
    @Column(name = "deptID")
    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    @Basic
    @Column(name = "userOAEmail")
    public String getUserOaEmail() {
        return userOaEmail;
    }

    public void setUserOaEmail(String userOaEmail) {
        this.userOaEmail = userOaEmail;
    }

    @Basic
    @Column(name = "userOrdinaryEmail")
    public String getUserOrdinaryEmail() {
        return userOrdinaryEmail;
    }

    public void setUserOrdinaryEmail(String userOrdinaryEmail) {
        this.userOrdinaryEmail = userOrdinaryEmail;
    }

    @Basic
    @Column(name = "userFullName")
    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    @Basic
    @Column(name = "userPhone")
    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    @Basic
    @Column(name = "userRight")
    public Short getUserRight() {
        return userRight;
    }

    public void setUserRight(Short userRight) {
        this.userRight = userRight;
    }

    @Basic
    @Column(name = "userPrimarySalt")
    public String getUserPrimarySalt() {
        return userPrimarySalt;
    }

    public void setUserPrimarySalt(String userPrimarySalt) {
        this.userPrimarySalt = userPrimarySalt;
    }

    @Basic
    @Column(name = "userStatus")
    public Short getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Short userStatus) {
        this.userStatus = userStatus;
    }

    @Basic
    @Column(name = "userCTime")
    public Timestamp getUserCTime() {
        return userCTime;
    }

    public void setUserCTime(Timestamp userCTime) {
        this.userCTime = userCTime;
    }

    @Basic
    @Column(name = "userUTime")
    public Timestamp getUserUTime() {
        return userUTime;
    }

    public void setUserUTime(Timestamp userUTime) {
        this.userUTime = userUTime;
    }

    @Basic
    @Column(name = "userDTime")
    public Timestamp getUserDTime() {
        return userDTime;
    }

    public void setUserDTime(Timestamp userDTime) {
        this.userDTime = userDTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEntity that = (UserEntity) o;

        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;
        if (userPass != null ? !userPass.equals(that.userPass) : that.userPass != null) return false;
        if (deptId != null ? !deptId.equals(that.deptId) : that.deptId != null) return false;
        if (userOaEmail != null ? !userOaEmail.equals(that.userOaEmail) : that.userOaEmail != null) return false;
        if (userOrdinaryEmail != null ? !userOrdinaryEmail.equals(that.userOrdinaryEmail) : that.userOrdinaryEmail != null)
            return false;
        if (userFullName != null ? !userFullName.equals(that.userFullName) : that.userFullName != null) return false;
        if (userPhone != null ? !userPhone.equals(that.userPhone) : that.userPhone != null) return false;
        if (userRight != null ? !userRight.equals(that.userRight) : that.userRight != null) return false;
        if (userPrimarySalt != null ? !userPrimarySalt.equals(that.userPrimarySalt) : that.userPrimarySalt != null)
            return false;
        if (userStatus != null ? !userStatus.equals(that.userStatus) : that.userStatus != null) return false;
        if (userCTime != null ? !userCTime.equals(that.userCTime) : that.userCTime != null) return false;
        if (userUTime != null ? !userUTime.equals(that.userUTime) : that.userUTime != null) return false;
        if (userDTime != null ? !userDTime.equals(that.userDTime) : that.userDTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (userPass != null ? userPass.hashCode() : 0);
        result = 31 * result + (deptId != null ? deptId.hashCode() : 0);
        result = 31 * result + (userOaEmail != null ? userOaEmail.hashCode() : 0);
        result = 31 * result + (userOrdinaryEmail != null ? userOrdinaryEmail.hashCode() : 0);
        result = 31 * result + (userFullName != null ? userFullName.hashCode() : 0);
        result = 31 * result + (userPhone != null ? userPhone.hashCode() : 0);
        result = 31 * result + (userRight != null ? userRight.hashCode() : 0);
        result = 31 * result + (userPrimarySalt != null ? userPrimarySalt.hashCode() : 0);
        result = 31 * result + (userStatus != null ? userStatus.hashCode() : 0);
        result = 31 * result + (userCTime != null ? userCTime.hashCode() : 0);
        result = 31 * result + (userUTime != null ? userUTime.hashCode() : 0);
        result = 31 * result + (userDTime != null ? userDTime.hashCode() : 0);
        return result;
    }
}
