package com.yncmcc.Entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by CMCC-Sunmo on 2016/9/15.
 */
@Entity
@Table(name = "admin", schema = "contractms", catalog = "")
public class AdminEntity {
    private String adminId;
    private String adminName;
    private String adminPass;
    private String adminPrimarySalt;
    private Short adminStatus;
    private Timestamp adminCTime;
    private Timestamp adminUTime;
    private Timestamp adminDTime;

    @Id
    @Column(name = "adminID")
    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    @Basic
    @Column(name = "adminName")
    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    @Basic
    @Column(name = "adminPass")
    public String getAdminPass() {
        return adminPass;
    }

    public void setAdminPass(String adminPass) {
        this.adminPass = adminPass;
    }

    @Basic
    @Column(name = "adminPrimarySalt")
    public String getAdminPrimarySalt() {
        return adminPrimarySalt;
    }

    public void setAdminPrimarySalt(String adminPrimarySalt) {
        this.adminPrimarySalt = adminPrimarySalt;
    }

    @Basic
    @Column(name = "adminStatus")
    public Short getAdminStatus() {
        return adminStatus;
    }

    public void setAdminStatus(Short adminStatus) {
        this.adminStatus = adminStatus;
    }

    @Basic
    @Column(name = "adminCTime")
    public Timestamp getAdminCTime() {
        return adminCTime;
    }

    public void setAdminCTime(Timestamp adminCTime) {
        this.adminCTime = adminCTime;
    }

    @Basic
    @Column(name = "adminUTime")
    public Timestamp getAdminUTime() {
        return adminUTime;
    }

    public void setAdminUTime(Timestamp adminUTime) {
        this.adminUTime = adminUTime;
    }

    @Basic
    @Column(name = "adminDTime")
    public Timestamp getAdminDTime() {
        return adminDTime;
    }

    public void setAdminDTime(Timestamp adminDTime) {
        this.adminDTime = adminDTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdminEntity that = (AdminEntity) o;

        if (adminId != null ? !adminId.equals(that.adminId) : that.adminId != null) return false;
        if (adminName != null ? !adminName.equals(that.adminName) : that.adminName != null) return false;
        if (adminPass != null ? !adminPass.equals(that.adminPass) : that.adminPass != null) return false;
        if (adminPrimarySalt != null ? !adminPrimarySalt.equals(that.adminPrimarySalt) : that.adminPrimarySalt != null)
            return false;
        if (adminStatus != null ? !adminStatus.equals(that.adminStatus) : that.adminStatus != null) return false;
        if (adminCTime != null ? !adminCTime.equals(that.adminCTime) : that.adminCTime != null) return false;
        if (adminUTime != null ? !adminUTime.equals(that.adminUTime) : that.adminUTime != null) return false;
        if (adminDTime != null ? !adminDTime.equals(that.adminDTime) : that.adminDTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = adminId != null ? adminId.hashCode() : 0;
        result = 31 * result + (adminName != null ? adminName.hashCode() : 0);
        result = 31 * result + (adminPass != null ? adminPass.hashCode() : 0);
        result = 31 * result + (adminPrimarySalt != null ? adminPrimarySalt.hashCode() : 0);
        result = 31 * result + (adminStatus != null ? adminStatus.hashCode() : 0);
        result = 31 * result + (adminCTime != null ? adminCTime.hashCode() : 0);
        result = 31 * result + (adminUTime != null ? adminUTime.hashCode() : 0);
        result = 31 * result + (adminDTime != null ? adminDTime.hashCode() : 0);
        return result;
    }
}
