package com.yncmcc.Entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by CMCC-Sunmo on 2016/9/15.
 */
@Entity
@Table(name = "department", schema = "contractms", catalog = "")
public class DepartmentEntity {
    private String deptId;
    private String deptName;
    private String deptDes;
    private Short deptStatus;
    private Timestamp deptCTime;
    private Timestamp deptUTime;
    private Timestamp deptDTime;

    @Id
    @Column(name = "deptID")
    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    @Basic
    @Column(name = "deptName")
    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    @Basic
    @Column(name = "deptDes")
    public String getDeptDes() {
        return deptDes;
    }

    public void setDeptDes(String deptDes) {
        this.deptDes = deptDes;
    }

    @Basic
    @Column(name = "deptStatus")
    public Short getDeptStatus() {
        return deptStatus;
    }

    public void setDeptStatus(Short deptStatus) {
        this.deptStatus = deptStatus;
    }

    @Basic
    @Column(name = "deptCTime")
    public Timestamp getDeptCTime() {
        return deptCTime;
    }

    public void setDeptCTime(Timestamp deptCTime) {
        this.deptCTime = deptCTime;
    }

    @Basic
    @Column(name = "deptUTime")
    public Timestamp getDeptUTime() {
        return deptUTime;
    }

    public void setDeptUTime(Timestamp deptUTime) {
        this.deptUTime = deptUTime;
    }

    @Basic
    @Column(name = "deptDTime")
    public Timestamp getDeptDTime() {
        return deptDTime;
    }

    public void setDeptDTime(Timestamp deptDTime) {
        this.deptDTime = deptDTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DepartmentEntity that = (DepartmentEntity) o;

        if (deptId != null ? !deptId.equals(that.deptId) : that.deptId != null) return false;
        if (deptName != null ? !deptName.equals(that.deptName) : that.deptName != null) return false;
        if (deptDes != null ? !deptDes.equals(that.deptDes) : that.deptDes != null) return false;
        if (deptStatus != null ? !deptStatus.equals(that.deptStatus) : that.deptStatus != null) return false;
        if (deptCTime != null ? !deptCTime.equals(that.deptCTime) : that.deptCTime != null) return false;
        if (deptUTime != null ? !deptUTime.equals(that.deptUTime) : that.deptUTime != null) return false;
        if (deptDTime != null ? !deptDTime.equals(that.deptDTime) : that.deptDTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = deptId != null ? deptId.hashCode() : 0;
        result = 31 * result + (deptName != null ? deptName.hashCode() : 0);
        result = 31 * result + (deptDes != null ? deptDes.hashCode() : 0);
        result = 31 * result + (deptStatus != null ? deptStatus.hashCode() : 0);
        result = 31 * result + (deptCTime != null ? deptCTime.hashCode() : 0);
        result = 31 * result + (deptUTime != null ? deptUTime.hashCode() : 0);
        result = 31 * result + (deptDTime != null ? deptDTime.hashCode() : 0);
        return result;
    }
}
