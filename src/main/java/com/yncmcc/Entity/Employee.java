package com.yncmcc.Entity;

import javax.persistence.*;

/**
 * Created by LiJiQiu on 2017/7/24.
 */
@Entity
@Table(name = "employee", schema = "contractms", catalog = "")
public class Employee {
    private String id;          //员工编号
    private String fullName;    //员工姓名
    private String email;       //邮箱

    @Id
    @Column(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fullName")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
