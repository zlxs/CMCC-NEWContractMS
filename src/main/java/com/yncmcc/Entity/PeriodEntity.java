package com.yncmcc.Entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by CMCC-Sunmo on 2016/9/15.
 */
@Entity
@Table(name = "period", schema = "contractms", catalog = "")
public class PeriodEntity {
    private String periodId;
    private String contractId;
    private Timestamp periodRemindTime;
    private Timestamp periodPaidTime;
    private Short periodIsWarn;
    private Double periodMoney;
    private Short periodStatus;
    private Timestamp periodCTime;
    private Timestamp periodUTime;
    private Timestamp periodDTime;

    @Id
    @Column(name = "periodID")
    public String getPeriodId() {
        return periodId;
    }

    public void setPeriodId(String periodId) {
        this.periodId = periodId;
    }

    @Basic
    @Column(name = "contractID")
    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    @Basic
    @Column(name = "periodRemindTime")
    public Timestamp getPeriodRemindTime() {
        return periodRemindTime;
    }

    public void setPeriodRemindTime(Timestamp periodRemindTime) {
        this.periodRemindTime = periodRemindTime;
    }

    @Basic
    @Column(name = "periodPaidTime")
    public Timestamp getPeriodPaidTime() {
        return periodPaidTime;
    }

    public void setPeriodPaidTime(Timestamp periodPaidTime) {
        this.periodPaidTime = periodPaidTime;
    }

    @Basic
    @Column(name = "periodIsWarn")
    public Short getPeriodIsWarn() {
        return periodIsWarn;
    }

    public void setPeriodIsWarn(Short periodIsWarn) {
        this.periodIsWarn = periodIsWarn;
    }

    @Basic
    @Column(name = "periodMoney")
    public Double getPeriodMoney() {
        return periodMoney;
    }

    public void setPeriodMoney(Double periodMoney) {
        this.periodMoney = periodMoney;
    }

    @Basic
    @Column(name = "periodStatus")
    public Short getPeriodStatus() {
        return periodStatus;
    }

    public void setPeriodStatus(Short periodStatus) {
        this.periodStatus = periodStatus;
    }

    @Basic
    @Column(name = "periodCTime")
    public Timestamp getPeriodCTime() {
        return periodCTime;
    }

    public void setPeriodCTime(Timestamp periodCTime) {
        this.periodCTime = periodCTime;
    }

    @Basic
    @Column(name = "periodUTime")
    public Timestamp getPeriodUTime() {
        return periodUTime;
    }

    public void setPeriodUTime(Timestamp periodUTime) {
        this.periodUTime = periodUTime;
    }

    @Basic
    @Column(name = "periodDTime")
    public Timestamp getPeriodDTime() {
        return periodDTime;
    }

    public void setPeriodDTime(Timestamp periodDTime) {
        this.periodDTime = periodDTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PeriodEntity that = (PeriodEntity) o;

        if (periodId != null ? !periodId.equals(that.periodId) : that.periodId != null) return false;
        if (contractId != null ? !contractId.equals(that.contractId) : that.contractId != null) return false;
        if (periodRemindTime != null ? !periodRemindTime.equals(that.periodRemindTime) : that.periodRemindTime != null)
            return false;
        if (periodPaidTime != null ? !periodPaidTime.equals(that.periodPaidTime) : that.periodPaidTime != null)
            return false;
        if (periodIsWarn != null ? !periodIsWarn.equals(that.periodIsWarn) : that.periodIsWarn != null) return false;
        if (periodMoney != null ? !periodMoney.equals(that.periodMoney) : that.periodMoney != null) return false;
        if (periodStatus != null ? !periodStatus.equals(that.periodStatus) : that.periodStatus != null) return false;
        if (periodCTime != null ? !periodCTime.equals(that.periodCTime) : that.periodCTime != null) return false;
        if (periodUTime != null ? !periodUTime.equals(that.periodUTime) : that.periodUTime != null) return false;
        if (periodDTime != null ? !periodDTime.equals(that.periodDTime) : that.periodDTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = periodId != null ? periodId.hashCode() : 0;
        result = 31 * result + (contractId != null ? contractId.hashCode() : 0);
        result = 31 * result + (periodRemindTime != null ? periodRemindTime.hashCode() : 0);
        result = 31 * result + (periodPaidTime != null ? periodPaidTime.hashCode() : 0);
        result = 31 * result + (periodIsWarn != null ? periodIsWarn.hashCode() : 0);
        result = 31 * result + (periodMoney != null ? periodMoney.hashCode() : 0);
        result = 31 * result + (periodStatus != null ? periodStatus.hashCode() : 0);
        result = 31 * result + (periodCTime != null ? periodCTime.hashCode() : 0);
        result = 31 * result + (periodUTime != null ? periodUTime.hashCode() : 0);
        result = 31 * result + (periodDTime != null ? periodDTime.hashCode() : 0);
        return result;
    }
}
