package com.yncmcc.Entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.sql.Timestamp;

/**
 * Created by CMCC-Sunmo on 2016/9/20.
 */
@Generated("org.hibernate.jpamodelgen.JPAMetaModelEntityProcesso")
@StaticMetamodel(ContractsEntity.class)
public class UserEntity_ {
    public static volatile SingularAttribute<ContractsEntity, String> userId;

    public static volatile SingularAttribute<ContractsEntity, String> userName;

    public static volatile SingularAttribute<ContractsEntity, String> userPass;
    public static volatile SingularAttribute<ContractsEntity, String> deptId;
    public static volatile SingularAttribute<ContractsEntity, String> userOaEmail;
    public static volatile SingularAttribute<ContractsEntity, String> userOrdinaryEmail;
    public static volatile SingularAttribute<ContractsEntity, String> userFullName;
    public static volatile SingularAttribute<ContractsEntity, String> userPhone;
    public static volatile SingularAttribute<ContractsEntity, Short> userRight;
    public static volatile SingularAttribute<ContractsEntity, String> userPrimarySalt;
    public static volatile SingularAttribute<ContractsEntity, Short> userStatus;
    public static volatile SingularAttribute<ContractsEntity, Timestamp> userCTime;
    public static volatile SingularAttribute<ContractsEntity, Timestamp> userUTime;
    public static volatile SingularAttribute<ContractsEntity, Timestamp> userDTime;

    public static volatile ListAttribute<ContractsEntity,UserEntity> contractsEntityList;
}

