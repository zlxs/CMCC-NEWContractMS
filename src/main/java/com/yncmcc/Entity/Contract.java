package com.yncmcc.Entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by LiJiQiu on 2017/7/24.
 */
@Entity
@Table(name = "contract", schema = "contractms", catalog = "")
public class Contract {
    private String contractId;     //合同编号
    private String contractName;   //合同名称
    private String dept;           //合同承办部门
    private String employeeName;   //合同拟稿人姓名
    private Stage  stage;          //合同所属阶段
    private Date   startDate;      //合同所属阶段开始时间

    @Id
    @Column(name = "contractId")
    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    @Basic
    @Column(name = "contractName")
    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    @Basic
    @Column(name = "dept")
    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    @Basic
    @Column(name = "employeeName")
    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    @Basic
    @Column(name = "stage")
    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Basic
    @Column(name = "startDate")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public enum Stage{
        ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6);

        private final int value;

        Stage(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        @Override
        public String toString() {
            return String.format("%s(%d)", this.name(), this.value);
        }

    }
}
