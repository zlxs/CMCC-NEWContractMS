package com.yncmcc.Entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by CMCC-Sunmo on 2016/9/15.
 */
@Entity
@Table(name = "logs", schema = "contractms", catalog = "")
public class LogsEntity {
    private String logId;
    private String userId;
    private String userIPforLog;
    private Short logOperRole;
    private String logOper;
    private Short logStatus;
    private Timestamp logCTime;
    private Timestamp logDTime;

    @Id
    @Column(name = "logID")
    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    @Basic
    @Column(name = "userID")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "userIPforLog")
    public String getUserIPforLog() {
        return userIPforLog;
    }

    public void setUserIPforLog(String userIPforLog) {
        this.userIPforLog = userIPforLog;
    }

    @Basic
    @Column(name = "logOperRole")
    public Short getLogOperRole() {
        return logOperRole;
    }

    public void setLogOperRole(Short logOperRole) {
        this.logOperRole = logOperRole;
    }

    @Basic
    @Column(name = "logOper")
    public String getLogOper() {
        return logOper;
    }

    public void setLogOper(String logOper) {
        this.logOper = logOper;
    }

    @Basic
    @Column(name = "logStatus")
    public Short getLogStatus() {
        return logStatus;
    }

    public void setLogStatus(Short logStatus) {
        this.logStatus = logStatus;
    }

    @Basic
    @Column(name = "logCTime")
    public Timestamp getLogCTime() {
        return logCTime;
    }

    public void setLogCTime(Timestamp logCTime) {
        this.logCTime = logCTime;
    }

    @Basic
    @Column(name = "logDTime")
    public Timestamp getLogDTime() {
        return logDTime;
    }

    public void setLogDTime(Timestamp logDTime) {
        this.logDTime = logDTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogsEntity that = (LogsEntity) o;

        if (logId != null ? !logId.equals(that.logId) : that.logId != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (userIPforLog != null ? !userIPforLog.equals(that.userIPforLog) : that.userIPforLog != null) return false;
        if (logOperRole != null ? !logOperRole.equals(that.logOperRole) : that.logOperRole != null) return false;
        if (logOper != null ? !logOper.equals(that.logOper) : that.logOper != null) return false;
        if (logStatus != null ? !logStatus.equals(that.logStatus) : that.logStatus != null) return false;
        if (logCTime != null ? !logCTime.equals(that.logCTime) : that.logCTime != null) return false;
        if (logDTime != null ? !logDTime.equals(that.logDTime) : that.logDTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = logId != null ? logId.hashCode() : 0;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (userIPforLog != null ? userIPforLog.hashCode() : 0);
        result = 31 * result + (logOperRole != null ? logOperRole.hashCode() : 0);
        result = 31 * result + (logOper != null ? logOper.hashCode() : 0);
        result = 31 * result + (logStatus != null ? logStatus.hashCode() : 0);
        result = 31 * result + (logCTime != null ? logCTime.hashCode() : 0);
        result = 31 * result + (logDTime != null ? logDTime.hashCode() : 0);
        return result;
    }
}
