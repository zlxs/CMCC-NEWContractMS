package com.yncmcc.Util;

/**
 * Created by Arabira on 2016/8/15.
 */
public class STATUS {
    public static int SUCCESS = 0;
    public static int FAIL = 1;
    public static int EXISTS = 2;
    public static int NOT_EXISTS = 3;
    public static int FORMAT_ERROR = 4;
    public static int ID_EMPTY = 5;
    public static int PAIDTIMEERROR = 6;
    public static int REMINDTIMEERROR = 7;
    public static int MONEYERROR = 8;
    public static int MINUS = 9;
    public static int STARTTIMEERROR = 10;
    public static int DEADLINEERROR = 11;
}
