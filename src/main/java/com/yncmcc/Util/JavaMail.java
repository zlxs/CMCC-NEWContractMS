package com.yncmcc.Util;

import com.yncmcc.Exception.MailAddressException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Arabira on 2016/8/9.
 * 使用此插件需要导入spring增强包（sring-context-support）,以及javaMail包（javax.mail）
 */
public class JavaMail {
    private JavaMailSenderImpl mailSender;
    private SimpleMailMessage message;
    private Properties properties;
    private String host;
    private String receiverAddr;
    private String senderAddr;
    private String mailSubject;
    private String content;
    private String senderUserName;
    private String senderPassword;
    private boolean isHtml;
    private String timeOut;

    public JavaMail() {
    }

    public void initSender(String host, String senderAddr, String senderUserName, String senderPassword) {
        this.host = host;
        this.senderAddr = senderAddr;
        this.senderUserName = senderUserName;
        this.senderPassword = senderPassword;
    }

    public void initReceiver(String receiverAddr, String mailSubject, String content, boolean isHtml, String timeOut) {
        this.receiverAddr = receiverAddr;
        this.mailSubject = mailSubject;
        this.content = content;
        this.isHtml = isHtml;
        this.timeOut = timeOut;
    }

    public void send() throws MailAddressException {
        mailSender = new JavaMailSenderImpl();
        message = new SimpleMailMessage();
        properties = new Properties();
        mailSender.setHost(host);
        mailSender.setUsername(senderUserName);
        mailSender.setPassword(senderPassword);
        if (checkMailAddress(senderAddr) && checkMailAddress(receiverAddr)) {
            message.setFrom(senderAddr);
            message.setTo(receiverAddr);
        } else {
            throw new MailAddressException("输入的邮件地址格式有误");
        }
        message.setSubject(mailSubject);
        message.setText(content);
        properties.put("mail.smtp.auth", isHtml);
        properties.put("mail.smtp.timeout", timeOut);
        mailSender.setJavaMailProperties(properties);
        System.out.println("发送邮件到：" + receiverAddr);
        mailSender.send(message);
    }

    public static boolean checkMailAddress(String address) {
//        String patternStr = "^(\\d|\\w|\\-|_){3,}@([0-9a-z_\\-]*)(\\.(com|cn|inc|org|cc|edu|de)*){1,2}([a-z]{2})?$";
        String patternStr = "[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[\\w](?:[\\w-]*[\\w])?";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(address);
        if (!matcher.find()) {
            return false;
        } else {
            return true;
        }
    }

    public static void main(String[] args) {
        String email = "18788176550@139.com";
        System.out.println(checkMailAddress(email));
    }
}
