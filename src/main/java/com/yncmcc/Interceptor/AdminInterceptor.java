package com.yncmcc.Interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by CMCC-Sunmo on 2016/9/27.
 */
public class AdminInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        String[] adminAction=new String[]{"/admin/addContract","/admin/addPeriod","/admin/getContractByID","/admin/getPeriodByCID","/admin/addContractByExcel","/admin/findContract","/admin/updateContract","/admin/deleteContract","/admin/addUser","/admin/addUserByExcel","/admin/findUser","/admin/getUserInfo","/admin/updateUserInfo","/admin/resetUserPass","/admin/deleteUser","/admin/findLog","/admin/deleteLog","/admin/addAdmin","/admin/getSelfInfo","/admin/UpdatePass"};
        HttpSession session = request.getSession();
        Object adminObj = session.getAttribute("adminId");
        String actionUrl = request.getRequestURL().toString();

        if(adminObj == null){
            response.sendRedirect(request.getContextPath()+"/login.jsp");
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
