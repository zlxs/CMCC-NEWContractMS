package com.yncmcc.Service;

import com.yncmcc.Entity.AdminEntity;
import com.yncmcc.Entity.ContractsEntity;
import com.yncmcc.Entity.PeriodEntity;
import com.yncmcc.Entity.UserEntity;
import com.yncmcc.Repository.Interface.AdminRepository;
import com.yncmcc.Repository.Interface.ContractsRepository;
import com.yncmcc.Repository.Interface.PeriodRepository;
import com.yncmcc.Repository.Interface.UserRepository;
import com.yncmcc.Service.Interface.AdminService;
import com.yncmcc.Service.Interface.ContractsService;
import com.yncmcc.Service.Interface.PeriodService;
import com.yncmcc.Service.Interface.UserService;
import com.yncmcc.Util.MD5Util;
import com.yncmcc.Util.STATUS;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by CMCC-sunmo on 2016/9/18.
 */
@Service("adminService")
@Transactional
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private PeriodRepository periodRepository;
    @Autowired
    private ContractsRepository contractsRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ContractsService contractsService;
    @Autowired
    private PeriodService periodService;
    @Autowired
    private UserService userService;
    @Override
    public List<AdminEntity> findAdminAll() {
        List list = adminRepository.findAll();
        return list;
    }

    @Override
    public AdminEntity findAdminById(String adminId) {
        return adminRepository.findOne(adminId);
    }

    @Override
    public String updateAdmin(AdminEntity adminEntity) {
        String message = "ok";
        AdminEntity admin = adminRepository.findOne(adminEntity.getAdminId());
        admin.setAdminName((adminEntity.getAdminName() != null) ? adminEntity.getAdminName() : admin.getAdminName());
        admin.setAdminStatus((adminEntity.getAdminStatus() != null) ? adminEntity.getAdminStatus() : admin.getAdminStatus());
        admin.setAdminPrimarySalt((adminEntity.getAdminPrimarySalt() != null) ? adminEntity.getAdminPrimarySalt() : admin.getAdminPrimarySalt());
        admin.setAdminCTime((adminEntity.getAdminCTime() != null) ? adminEntity.getAdminCTime() : admin.getAdminCTime());
        admin.setAdminDTime((adminEntity.getAdminDTime() != null) ? adminEntity.getAdminDTime() : admin.getAdminDTime());
        if(!StringUtils.isBlank(adminEntity.getAdminPass())){
            String pass = MD5Util.calc(adminEntity.getAdminPass() + admin.getAdminPrimarySalt());
            admin.setAdminPass(pass);
        }
        adminEntity.setAdminUTime(new Timestamp(System.currentTimeMillis()));
        adminRepository.saveAndFlush(admin);
        return message;
    }

    @Override
    public String addAdmin(AdminEntity adminEntity) {
        String message = "ok";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        int random = (int)(Math.random()*10000);
        System.out.print("random : " + random);
        adminEntity.setAdminId(sdf.format(new Date()) + random + "");
        adminEntity.setAdminPrimarySalt(random + "");
        adminEntity.setAdminPass(MD5Util.calc(adminEntity.getAdminPass() + random));
        adminEntity.setAdminCTime(new Timestamp(System.currentTimeMillis()));
        adminEntity.setAdminStatus((short) 1);
        adminRepository.save(adminEntity);
        return message;
    }

    @Override
    public AdminEntity findByAdminName(String adminName) {
        return adminRepository.findByAdminName(adminName);
    }

    @Override
    public boolean checkPass(String adminId, String password) {
        boolean flag = false;
        AdminEntity check = adminRepository.findOne(adminId);
        String pass = MD5Util.calc(password + check.getAdminPrimarySalt());
//        String pass = password;
        if(pass.equals(check.getAdminPass())){
            flag = true;
        }
        return flag;
    }


}