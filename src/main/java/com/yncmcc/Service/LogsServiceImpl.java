package com.yncmcc.Service;

import com.yncmcc.Entity.LogsCustomEntity;
import com.yncmcc.Entity.LogsEntity;
import com.yncmcc.Repository.Interface.LogsRepository;
import com.yncmcc.Service.Interface.LogsService;
import com.yncmcc.Util.STATUS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by CMCC-sunmo on 2016/9/18.
 */
@Service("logsService")
@Transactional
public class LogsServiceImpl implements LogsService {

    @Autowired
    private LogsRepository logsRepository;
    private Map<String, Object> response;

    @Override
    public List<LogsCustomEntity> findLog(LogsEntity logsEntity) {
        List<LogsCustomEntity> logsEntities = logsRepository.findLogs(logsEntity);
        return logsEntities;
    }

    @Override
    public LogsEntity findByLogId(String logId) {
        LogsEntity logsEntity = logsRepository.findByLogID(logId);
        return logsEntity;
    }

    @Override
    public String deleteLog(String logId) {
        String message = "ok";
        LogsEntity check = logsRepository.findByLogID(logId);
        if(null == check){
            message = "删除的日志不存在";
            return message;
        }
        LogsEntity logsEntity = logsRepository.findByLogID(logId);
        logsEntity.setLogStatus((Short) (short) 0);
        logsRepository.saveAndFlush(logsEntity);
        return message;
    }

    @Override
    public String saveLog(String userId,String userIPforLog,Short logOperRole,String logOper) {
        String message = "ok";
        LogsEntity logsEntity = new LogsEntity();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        int random = (int)(Math.random()*10000);
        logsEntity.setLogId(sdf.format(new java.util.Date()) + random + "");
        logsEntity.setUserId(userId);
        logsEntity.setUserIPforLog(userIPforLog);
        logsEntity.setLogOperRole(logOperRole);
        logsEntity.setLogOper(logOper);
        logsEntity.setLogStatus((Short)(short)1);
        Date date = new Date();
        logsEntity.setLogCTime(new Timestamp(date.getTime()));
        logsRepository.saveAndFlush(logsEntity);
        return message;
    }
}
