package com.yncmcc.Service.Interface;

import com.yncmcc.Entity.ContractsCustomEntity;
import com.yncmcc.Entity.ContractsEntity;
import jxl.read.biff.BiffException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * Created by CMCC-sunmo on 2016/9/18.
 */
public interface ContractsService {
    public ContractsEntity findByContractID(String contractId);

    public String addContract(ContractsEntity contractsEntity);
    public ContractsEntity findByContractSerial(String contractSerial);

    public int updateContract(ContractsEntity contractsEntity);
    public String addByExcel(MultipartFile file) throws IOException, BiffException, ParseException;


    public List<ContractsCustomEntity> findReqContract(ContractsEntity contractsEntity);
    public String deleteContract(String contractId);

    public List<ContractsCustomEntity> findBySrRemindTime();
    public List<ContractsCustomEntity> findByEaRemindTime();
    public List<ContractsCustomEntity> findByCtRemindTime();
}
