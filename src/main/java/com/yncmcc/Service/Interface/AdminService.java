package com.yncmcc.Service.Interface;


import com.yncmcc.Entity.AdminEntity;
import com.yncmcc.Entity.ContractsEntity;
import com.yncmcc.Entity.PeriodEntity;
import jxl.read.biff.BiffException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Created by CMCC-sunmo on 2016/9/18.
 */
public interface AdminService {
    public List<AdminEntity> findAdminAll();
    public AdminEntity findAdminById(String adminId);
    public String updateAdmin(AdminEntity adminEntity);
    public String addAdmin(AdminEntity adminEntity);
    public AdminEntity findByAdminName(String adminName);
    public boolean checkPass(String adminId , String password);
}
