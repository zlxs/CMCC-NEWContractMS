package com.yncmcc.Service.Interface;

import com.yncmcc.Entity.ContractsEntity;
import com.yncmcc.Entity.DepartmentEntity;
import com.yncmcc.Entity.UserEntity;
import jxl.read.biff.BiffException;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by CMCC-sunmo on 2016/9/18.
 */
public interface UserService {

    public UserEntity findByUserId(String userId);
    public UserEntity findByUserName(String userName);
    public List<DepartmentEntity> findDepartmentOfAll();
    public List<UserEntity> findUser(UserEntity userEntity);
    public String deleteUser(String userId);
    public String updateUser(UserEntity userEntity);
    public String addUser(UserEntity userEntity);
    public boolean checkPass(String userId, String password);
    public String addUserByExcel(MultipartFile file) throws IOException, BiffException;
    public boolean checkUser(UserEntity userEntity);
}
