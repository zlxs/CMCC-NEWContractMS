package com.yncmcc.Service.Interface;

import com.yncmcc.Entity.LogsCustomEntity;
import com.yncmcc.Entity.LogsEntity;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * Created by CMCC-sunmo on 2016/9/18.
 */
public interface LogsService {
    public List<LogsCustomEntity> findLog(LogsEntity logsEntity);
    public LogsEntity findByLogId(String logId);
    public String deleteLog(String logId);

    public String saveLog(String userId,String userIPforLog,Short logOperRole,String logOper);
}
