package com.yncmcc.Service.Interface;

import com.yncmcc.Entity.DepartmentEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by CMCC on 2016/9/22.
 */
public interface DepartmentService {
    public DepartmentEntity findByDepartmentName(String departmentName);
}
