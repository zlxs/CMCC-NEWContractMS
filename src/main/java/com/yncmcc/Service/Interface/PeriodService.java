package com.yncmcc.Service.Interface;

import com.yncmcc.Entity.PeriodEntity;
import jxl.read.biff.BiffException;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by CMCC-sunmo on 2016/9/18.
 */
public interface PeriodService {

    public PeriodEntity[] findByRemindDate();
    public String addPeriod(PeriodEntity periodEntity);
    public String updatePeriod(PeriodEntity periodEntity);
    public List<PeriodEntity> findByContractId(String contractId);
    public String deletePeriodByContractId(String contractId);
    public String addByExcel(MultipartFile file) throws IOException, BiffException;
}
