package com.yncmcc.Service;

import com.yncmcc.Entity.DepartmentEntity;
import com.yncmcc.Repository.Interface.DepartmentRepository;
import com.yncmcc.Service.Interface.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by CMCC on 2016/9/22.
 */
@Service("departmentService")
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public DepartmentEntity findByDepartmentName(String departmentName) {
        DepartmentEntity departmentEntity = departmentRepository.findByDepartmentName(departmentName);
        return departmentEntity;
    }
}
