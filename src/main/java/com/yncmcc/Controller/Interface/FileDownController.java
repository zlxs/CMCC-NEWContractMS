package com.yncmcc.Controller.Interface;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by CMCC-Sunmo on 2016/9/9.
 */
public interface FileDownController {
    public String fileDownload(String fileName, HttpServletRequest request,HttpServletResponse response) throws IOException;
}
