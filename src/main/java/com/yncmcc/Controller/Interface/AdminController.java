package com.yncmcc.Controller.Interface;

import com.yncmcc.Entity.*;
import jxl.read.biff.BiffException;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

/**
 * Created by CMCC-sunmo on 2016/9/18.
 */
public interface AdminController {
    public Map<String , Object>addContract(ContractsEntity contractsEntity, HttpServletRequest request, HttpSession session);
    public Map<String , Object>addPeriod(PeriodEntity periodEntity, HttpServletRequest request, HttpSession session);
    public Map<String , Object>getContractByID(String contractId, HttpServletRequest request, HttpSession session);
    public Map<String , Object>getPeriodByCID(String contractId, HttpServletRequest request, HttpSession session);
    public Map<String, Object> addContractByExcel(MultipartFile file, HttpServletRequest request, HttpSession session) throws IOException, BiffException, ParseException;
    public Map<String , Object>findContract(ContractsEntity contractsEntity, HttpServletRequest request, HttpSession session);
    public Map<String, Object> updateContract(ContractsEntity contractEntity, HttpServletRequest request, HttpSession session);
    public Map<String, Object> deleteContract(String ContractId, HttpServletRequest request, HttpSession session);
    public Map<String, Object> addUser(UserEntity userEntity, HttpServletRequest request, HttpSession session);
    public Map<String, Object> addUserByExcel(MultipartFile file, HttpServletRequest request, HttpSession session) throws IOException, BiffException, ParseException;
    public Map<String , Object>findUser(UserEntity userEntity, HttpServletRequest request, HttpSession session);
    public Map<String , Object>getUser(String userId, HttpServletRequest request, HttpSession session);
    public Map<String , Object>updateUser(UserEntity userEntity, HttpServletRequest request, HttpSession session);
    public Map<String , Object>resetUserPass(String userId, String newPass, HttpServletRequest request, HttpSession session);
    public Map<String , Object>deleteUser(String userId, HttpServletRequest request, HttpSession session);
    public Map<String , Object>findLog(LogsEntity logEntity, HttpServletRequest request, HttpSession session);
    public Map<String , Object>deleteLog(String logId, HttpServletRequest request, HttpSession session);
    public Map<String , Object>addAdmin(String adminName, String adminPass, HttpServletRequest request, HttpSession session);
    public Map<String , Object>getSelfInfo(HttpServletRequest request, HttpSession session);
    public Map<String , Object>updatePass(String adminId, String oldPassword, String newPassword);
    public Map<String, Object> updatePeriod(PeriodEntity periodEntity, HttpServletRequest request, HttpSession session);
    public String errorAction();
}
