package com.yncmcc.Controller.Interface;

import com.yncmcc.Entity.ContractsEntity;
import com.yncmcc.Entity.PeriodEntity;
import jxl.read.biff.BiffException;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

/**
 * Created by CMCC-sunmo on 2016/9/18.
 */
public interface UserController {


    /*修改合同的方法*/
    public Map<String, Object> updateContract(ContractsEntity contractEntity, HttpServletRequest request, HttpSession session);
    /*批量导入合同的方法*/
    public Map<String, Object> addContractByExcel(MultipartFile file, HttpServletRequest request, HttpSession session) throws IOException, BiffException, ParseException;
    public Map<String , Object>login(String userName, String userPass, HttpServletRequest request, HttpSession session);
    public Map<String , Object>logout(HttpServletRequest request, HttpSession session);
    public Map<String , Object>addContract(ContractsEntity contractsEntity, HttpServletRequest request, HttpSession session);
    public Map<String , Object>addPeriod(PeriodEntity periodEntity, HttpServletRequest request, HttpSession session);
    public Map<String , Object>getContractByID(String contractId, HttpServletRequest request, HttpSession session);
    public Map<String , Object>getPeriodByCID(String contractId, HttpServletRequest request, HttpSession session);
    public Map<String , Object>findContract(ContractsEntity contractsEntity, HttpServletRequest request, HttpSession session);
    public Map<String , Object>getSelfInfo(String userId, HttpServletRequest request, HttpSession session);
    public Map<String , Object>updatePass(String userId, String oldPassword, String newPassword,  HttpServletRequest request, HttpSession session);
    public Map<String , Object>updateEmail(String email, String type, String userId, HttpServletRequest request, HttpSession session);
    public Map<String , Object>updatePeriod(PeriodEntity periodEntity, HttpServletRequest request, HttpSession session);
    public String errorAction();
}
