package com.yncmcc.Filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by CMCC-Sunmo on 2016/9/27.
 */
public class userFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest hrequest = (HttpServletRequest) request;
        HttpServletResponse hresponse = (HttpServletResponse) response;
        HttpSession session = hrequest.getSession();
        Object userObj = session.getAttribute("userId");
        Object adminObj = session.getAttribute("adminId");
        Object userRight = session.getAttribute("userRight");
        String url = hrequest.getRequestURL().toString();

        if(url.indexOf("/user")>0){
            if(url.indexOf("/user/Contract.jsp")>0){
                hresponse.sendRedirect(hrequest.getContextPath()+"/user/UserContract.jsp");
                return ;
            }
            if(url.indexOf("/user/ContractByDept.jsp")>0){
                hresponse.sendRedirect(hrequest.getContextPath()+"/user/DeptContract.jsp");
                return ;
            }

            if(userObj==null && adminObj==null){
                hresponse.sendRedirect(hrequest.getContextPath()+"/login.jsp");
                return ;
            }else if(userObj==null && adminObj !=null){
                hresponse.setCharacterEncoding("UTF-8");
                hresponse.setHeader("content-type", "text/html;charset=UTF-8");
                PrintWriter out = response.getWriter();
                out.write("<script language=javascript>alert('您无权访问此页面，将跳转至个人信息页面！');window.location.href='"+hrequest.getContextPath()+"/admin/AdminSelf.jsp'</script>");
//                hresponse.sendRedirect(hrequest.getContextPath()+"/admin/AdminSelf.jsp");
                return ;
            }else{
                if(url.indexOf("/user/DeptContract.jsp")>0 && userRight.toString().equals("3")){
                    hresponse.setCharacterEncoding("UTF-8");
                    hresponse.setHeader("content-type", "text/html;charset=UTF-8");
                    PrintWriter out = response.getWriter();
                    out.write("<script language=javascript>alert('您无权访问此页面，将跳转至登录页面！');window.location.href='"+hrequest.getContextPath()+"/login.jsp'</script>");
                }
                chain.doFilter(request,response);
                return ;
            }

        }else if(url.indexOf("/admin")>0 || url.indexOf("/view")>0){
            if(url.indexOf("/admin/Contract.jsp")>0){
                hresponse.sendRedirect(hrequest.getContextPath()+"/admin/AdminContract.jsp");
                return ;
            }
            if(url.indexOf("/admin/Log.jsp")>0){
                hresponse.sendRedirect(hrequest.getContextPath()+"/admin/AdminLog.jsp");
                return ;
            }
            if(url.indexOf("/admin/User.jsp")>0){
                hresponse.sendRedirect(hrequest.getContextPath()+"/admin/AdminUser.jsp");
                return ;
            }

            if(userObj==null && adminObj==null){
                hresponse.sendRedirect(hrequest.getContextPath()+"/login.jsp");
                return ;
            }else if(userObj!=null && adminObj ==null){
                hresponse.setCharacterEncoding("UTF-8");
                hresponse.setHeader("content-type", "text/html;charset=UTF-8");
                PrintWriter out = response.getWriter();
                out.write("<script language=javascript>alert('您无权访问此页面，将跳转至个人信息页面！');window.location.href='"+hrequest.getContextPath()+"/user/UserSelf.jsp'</script>");

//                hresponse.sendRedirect(hrequest.getContextPath()+"/user/UserSelf.jsp");
                return ;
            }
            chain.doFilter(request,response);
        }else{
            chain.doFilter(request,response);
        }


    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
