package com.yncmcc.Filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by CMCC-Sunmo on 2016/9/27.
 */
public class adminFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest hrequest = (HttpServletRequest) request;
        HttpServletResponse hresponse = (HttpServletResponse) response;
        HttpSession session = hrequest.getSession();
        Object userObj = session.getAttribute("userId");
        Object adminObj = session.getAttribute("adminId");
        Object userRight = session.getAttribute("userRight");
        String url = hrequest.getRequestURL().toString();

        if(url.indexOf("/admin/Contract.jsp")>0){
            hresponse.sendRedirect(hrequest.getContextPath()+"/admin/AdminContract.jsp");
            return ;
        }
        if(url.indexOf("/admin/Log.jsp")>0){
            hresponse.sendRedirect(hrequest.getContextPath()+"/admin/AdminLog.jsp");
            return ;
        }
        if(url.indexOf("/admin/User.jsp")>0){
            hresponse.sendRedirect(hrequest.getContextPath()+"/admin/AdminUser.jsp");
            return ;
        }

        if(userObj==null && adminObj==null){
            hresponse.sendRedirect(hrequest.getContextPath()+"/login.jsp");
            return ;
        }else if(userObj!=null && adminObj ==null){
            hresponse.sendRedirect(hrequest.getContextPath()+"/user/UserSelf.jsp");
            return ;
        }


    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
