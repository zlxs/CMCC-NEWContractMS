package com.yncmcc.Log;

import com.yncmcc.Entity.ContractsEntity;
import com.yncmcc.Entity.LogsEntity;
import com.yncmcc.Entity.PeriodEntity;
import com.yncmcc.Entity.UserEntity;
import com.yncmcc.Service.Interface.AdminService;
import com.yncmcc.Service.Interface.ContractsService;
import com.yncmcc.Service.Interface.LogsService;
import com.yncmcc.Service.Interface.UserService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by CMCC-Sunmo on 2016/9/18.
 */
@Aspect
@Component
public class AdminLog {
    private static String AdminDomain = "管理员";
    private static Short AdminRight = 0;
    @Autowired
    private UserService userService;
    @Autowired
    private AdminService adminService;
    @Autowired
    private ContractsService contractsService;
    @Autowired
    private LogsService Log;

    @Around("execution(* com.yncmcc.Controller.AdminControllerImpl.*Contract(..))")
    public Object adminContractLog(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] value = joinPoint.getArgs();
        HttpSession session = (HttpSession) value[value.length - 1];
        HttpServletRequest request = (HttpServletRequest) value[value.length - 2];
        String adminId = (String) session.getAttribute("adminId");
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        Map<String, Object> response = (Map<String, Object>) joinPoint.proceed();
        ContractsEntity contractsEntity = null;


        switch (method.getName()){
            case "findContract":
                contractsEntity = (ContractsEntity) value[0];
                Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员查找合同（deptId:"+contractsEntity.getDeptId()+"，userName:"+contractsEntity.getUserId()+"，contractSerial:"+contractsEntity.getContractSerial()+"，contractName:"+contractsEntity.getContractName()+"，contractCate:"+contractsEntity.getContractCate()+")"+response.get("des"));
                break;
            case "addContract":
                contractsEntity = (ContractsEntity) value[0];
                Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员添加合同（contractSerial:"+contractsEntity.getContractSerial()+"，contractName:"+contractsEntity.getContractName()+"，contractCate:"+contractsEntity.getContractCate()+")"+response.get("des"));
                break;
            case "updateContract":
                contractsEntity = (ContractsEntity) value[0];
                Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员更新合同（contractSerial:"+contractsEntity.getContractSerial()+"，contractName:"+contractsEntity.getContractName()+"，contractCate:"+contractsEntity.getContractCate()+")"+response.get("des"));
                break;
            case "deleteContract":
                String contractId = (String) value[0];
                Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员删除合同（contractId:"+contractId+")"+response.get("des"));
                break;
        }

        return response;
    }

    @Around("execution(* com.yncmcc.Controller.AdminControllerImpl.addPeriod(..))")
    public Object adminAddPeriodLog(ProceedingJoinPoint joinPoint) throws Throwable{
        Object[] value = joinPoint.getArgs();
        HttpSession session = (HttpSession) value[value.length - 1];
        HttpServletRequest request = (HttpServletRequest) value[value.length - 2];
        String adminId = (String) session.getAttribute("adminId");
        PeriodEntity periodEntity = (PeriodEntity) value[0];
        Map<String, Object> response = (Map<String, Object>) joinPoint.proceed();

        Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员添加分期（contractID:"+periodEntity.getContractId()+")"+response.get("des"));

        return response;
    }

    @Around("execution(* com.yncmcc.Controller.AdminControllerImpl.getPeriodByCID(..))")
    public Object adminGetContractByIDLog(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] value = joinPoint.getArgs();
        HttpSession session = (HttpSession) value[value.length - 1];
        HttpServletRequest request = (HttpServletRequest) value[value.length - 2];
        String adminId = (String) session.getAttribute("adminId");
        String contractId = (String) value[0];
        Map<String, Object> response = (Map<String, Object>) joinPoint.proceed();

        Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员通过合同ID查找合同（contractID:"+contractId+")"+response.get("des"));

        return response;
    }

    @Around("execution(* com.yncmcc.Controller.AdminControllerImpl.addContractByExcel(..))")
    public Object adminAddContractByExcelLog(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] value = joinPoint.getArgs();
        HttpSession session = (HttpSession) value[value.length - 1];
        HttpServletRequest request = (HttpServletRequest) value[value.length - 2];
        String adminId = (String) session.getAttribute("adminId");

        Map<String, Object> response = (Map<String, Object>) joinPoint.proceed();

        Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员批量导入合同"+response.get("des"));

        return response;
    }

    @Around("execution(* com.yncmcc.Controller.AdminControllerImpl.*User(..))")
    public Object adminUserLog(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] value = joinPoint.getArgs();
        HttpSession session = (HttpSession) value[value.length - 1];
        HttpServletRequest request = (HttpServletRequest) value[value.length - 2];
        String adminId = (String) session.getAttribute("adminId");
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        Map<String, Object> response = (Map<String, Object>) joinPoint.proceed();
        UserEntity userEntity = null;
        String userId = "";

        switch (method.getName()){
            case "findUser":
                userEntity = (UserEntity) value[0];
                Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员查找用户（userName:"+userEntity.getUserName()+"，deptId:"+userEntity.getDeptId()+"，userFallName:"+userEntity.getUserFullName()+"，userRight:"+userEntity.getUserRight()+")"+response.get("des"));
                break;
            case "addUser":
                userEntity = (UserEntity) value[0];
                Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员添加用户（userName:"+userEntity.getUserName()+"，userOaEmail:"+userEntity.getUserOaEmail()+"，userOrdinaryEmail:"+userEntity.getUserOrdinaryEmail()+")"+response.get("des"));
                break;
            case "updateUser":
                userEntity = (UserEntity) value[0];
                Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员更新用户（deptId:"+userEntity.getDeptId()+"，userOaEmail:"+userEntity.getUserOaEmail()+"，userOrdinaryEmail:"+userEntity.getUserOrdinaryEmail()+",userPhone:" + userEntity.getUserRight()+",userRight"+userEntity.getUserRight()+")"+response.get("des"));
                break;
            case "getUser":
                userId = (String) value[0];
                Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员获得用户（userId:"+userId+")"+response.get("des"));
                break;
            case "deleteUser":
                userId = (String) value[0];
                Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员删除用户（userId:"+userId+")"+response.get("des"));
                break;
        }

        return response;
    }

    @Around("execution(* com.yncmcc.Controller.AdminControllerImpl.resetUserPass(..))")
    public Object adminResetUserPassLog(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] value = joinPoint.getArgs();
        HttpSession session = (HttpSession) value[value.length - 1];
        HttpServletRequest request = (HttpServletRequest) value[value.length - 2];
        String adminId = (String) session.getAttribute("adminId");
        String userId = (String) value[0];
        Map<String, Object> response = (Map<String, Object>) joinPoint.proceed();

        Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员重置用户密码(userId:"+userId + ")"+response.get("des"));

        return response;
    }

    @Around("execution(* com.yncmcc.Controller.AdminControllerImpl.*Log(..))")
    public Object adminLogLog(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] value = joinPoint.getArgs();
        HttpSession session = (HttpSession) value[value.length - 1];
        HttpServletRequest request = (HttpServletRequest) value[value.length - 2];
        String adminId = (String) session.getAttribute("adminId");
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        Map<String, Object> response = (Map<String, Object>) joinPoint.proceed();
        LogsEntity logsEntity = null;
        String logId = "";

        switch (method.getName()){
            case "findLog":
                logsEntity = (LogsEntity) value[0];
                Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员查找日志（userName:"+logsEntity.getUserId()+"，userIPforLog:"+logsEntity.getUserIPforLog()+"，logOperRole:"+logsEntity.getLogOperRole()+"，logOper:"+logsEntity.getLogOper()+ ",logCTime:"+logsEntity.getLogCTime()+response.get("des"));
                break;
            case "deleteLog":
                logId = (String) value[0];
                Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员删除日志（logId:"+logId+")"+response.get("des"));
                break;
        }

        return response;
    }

    @Around("execution(* com.yncmcc.Controller.AdminControllerImpl.addAdmin(..))")
    public Object adminAddAdminPassLog(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] value = joinPoint.getArgs();
        HttpSession session = (HttpSession) value[value.length - 1];
        HttpServletRequest request = (HttpServletRequest) value[value.length - 2];
        String adminId = (String) session.getAttribute("adminId");
        String adminName = (String) value[0];
        Map<String, Object> response = (Map<String, Object>) joinPoint.proceed();

        Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员添加管理员(adminName:"+adminName + ")"+response.get("des"));

        return response;
    }

    @Around("execution(* com.yncmcc.Controller.AdminControllerImpl.getSelfInfo(..))")
    public Object adminGetSelfInfoLog(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] value = joinPoint.getArgs();
        HttpSession session = (HttpSession) value[value.length - 1];
        HttpServletRequest request = (HttpServletRequest) value[value.length - 2];
        String adminId = (String) session.getAttribute("adminId");

        Map<String, Object> response = (Map<String, Object>) joinPoint.proceed();

        Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员查看个人信息(adminId:"+adminId + ")"+response.get("des"));

        return response;
    }

    @Around("execution(* com.yncmcc.Controller.AdminControllerImpl.updatePass(..))")
    public Object adminUpdatePassLog(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] value = joinPoint.getArgs();
        HttpSession session = (HttpSession) value[value.length - 1];
        HttpServletRequest request = (HttpServletRequest) value[value.length - 2];
        String adminId = (String) session.getAttribute("adminId");

        Map<String, Object> response = (Map<String, Object>) joinPoint.proceed();

        Log.saveLog(adminId,request.getRemoteAddr(),AdminRight,"管理员重置管理员密码(adminId:"+adminId + ")"+response.get("des"));

        return response;
    }


}
