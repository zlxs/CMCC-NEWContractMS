function createXmlHttpRequest() {
    if (window.ActiveXObject)
        return new ActiveXObject("Microsoft.XMLHTTP");
    else if (window.XMLHttpRequest)
        return new XMLHttpRequest();
}
//关闭子窗口
function close_child_window() {
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}