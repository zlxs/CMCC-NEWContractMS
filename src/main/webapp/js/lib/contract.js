
/**合同的相关操作*/
  var hosturl = "http://"+ window.location.host + "/"+ window.location.pathname.split('/')[1]+ "/" +window.location.pathname.split('/')[2];


  function simpleTemplating(data) {
        var html = '';
        $.each(data, function (index, item) {
			var contractCate = item.contractCate + "";
			var deptName = getDeptName(dept , item.deptId);
			html += '<tr>';
            html += '<td>'+ item.contractSerial +'</td>';
			html += '<td>'+ item.contractName +'</td>';
			html += '<td>'+ item.deptName +'</td>';
			html += '<td>'+ item.userName +'</td>';
			if(contractCate == "1"){
				var warnOrNor = (item.srIsWarn == "0")?"否":"是";
				var remindTime = (item.srRemindTime == null)?"":item.srRemindTime;
				html += '<td>'+ "呈批件" +'</td>';
				html += '<td>'+ "无" +'</td>';
				html += '<td>'+ getSmpFormatDateByLong(item.srStartTime , false) +'</td>';
				html += '<td>'+ getSmpFormatDateByLong(item.srEndTime , false) +'</td>';
				html += '<td>'+ warnOrNor +'</td>';
				if(warnOrNor == "是"){
					html += '<td>'+ getSmpFormatDateByLong(remindTime , false) +'</td>'	
				}else{
					html += '<td></td>'
				}
			}else if(contractCate == "2"){
				var warnOrNor = (item.eaIsWarn == "0")?"否":"是";
				var remindTime = (item.eaRemindTime == null)?"":item.eaRemindTime;
				html += '<td>'+ "审批表" +'</td>';
				html += '<td>'+ "无" +'</td>';
				html += '<td>'+ getSmpFormatDateByLong(item.eaStartTime , false) +'</td>';
				html += '<td>'+ getSmpFormatDateByLong(item.eaEndTime , false) +'</td>';
				html += '<td>'+ warnOrNor +'</td>';
				if(warnOrNor == "是"){
					html += '<td>'+ getSmpFormatDateByLong(remindTime , false) +'</td>'	
				}else{
					html += '<td></td>'
				}
			}else if(contractCate == "3"){
				var warnOrNor = (item.ctIsWarn == "0")?"否":"是";
				var remindTime = (item.ctRemindTime == null)?"":item.ctRemindTime;
				html += '<td>'+ "合同" +'</td>';
				html += '<td><a data-toggle="modal" data-target="#updatePeriod" onclick="setPeriod(\''+ item.contractId +'\','+ item.ctPeriodNum +')">'+ item.ctPeriodNum +'</a></td>';
				html += '<td>'+ getSmpFormatDateByLong(item.ctStartTime , false) +'</td>';
				html += '<td>'+ getSmpFormatDateByLong(item.ctEndTime ,false) +'</td>';
				html += '<td>'+ warnOrNor +'</td>';
				if(warnOrNor == "是"){
					html += '<td>'+ getSmpFormatDateByLong(remindTime , false) +'</td>'	
				}else{
					html += '<td></td>'
				}
			}
            html +=  '<td><button onclick="setContractDataForCtmodal(\''+ item.contractId +'\')" class="btn btn-primary" data-toggle="modal" data-target="#updateContract">修改</button></td>';
            html += '</tr>';
        });
        return html;
    };
	
	function updatePeriod(id){
		var PaidTime = document.getElementById("p" + id).value + " 00:00:00";
		var periodMoney = document.getElementById("m" + id).value + "";
		var periodRemindTime = "2000-01-01 00:00:00";
		var isWarn = "0";
		if($("#c"+id).is(":checked")){
			isWarn = "1";
			periodRemindTime = document.getElementById("r" + id).value + " 00:00:00"
		}
		var aj = $.ajax({
			url: hosturl + '/updatePeriod',
			data:{
				periodId:id + "",
				periodIsWarn:isWarn,
				periodPaidTime:PaidTime,
				periodMoney:periodMoney,
				periodIsWarn:isWarn,
				periodRemindTime:periodRemindTime
			},
			type:"POST",
			dataType:"json",
			success:function(data){
				if(data.status == "ok"){
					alert(data.des);
//					setTimeout(function(){window.location.reload(true);},1000);
				}else{
					alert(data.des);
				}
			}
		});
	}
	
	function setPeriod(id , num){
//		$('#updatePeriod').modal({backdrop: 'static', keyboard: false});
		$("#per_row").html("");
		var aj = $.ajax({
			url:hosturl + '/getPeriodByCID',
			data:{
				contractId:id+""
			},
			type:"GET",
			dataType:"json",
			success:function(data){
				if(data.status == "ok"){
						$.each(data.data, function (index, item) {
						var html = '<div class="row"><div class="col-md-3"><input required id="p'+ item.periodId +'" class="form-control " type="date" placeholder="付款时间" value="'+ getSmpFormatDateByLong(item.periodPaidTime , false) +'" ></div>';
						html = html + '<div class="col-md-2"><input required id="m' +    item.periodId +'" class="form-control" type="text" placeholder="付款金额" value="' + item.periodMoney + '" ></div>';
						var isWarn = item.periodIsWarn + "";
						if(isWarn == "1"){
							html = html + '<div class="col-md-1"><input required id="c' + item.periodId +'" class="form-control" type="checkbox" checked="checked"></div>';
							html = html + '<div class="col-md-3"><input required id="r' + item.periodId +'" class="form-control" type="date" placeholder="提醒时间" value="' + getSmpFormatDateByLong(item.periodRemindTime , false) + '" ></div>';
						}else{
							html = html + '<div class="col-md-1"><input required id="c' + item.periodId +'" class="form-control" type="checkbox" ></div>';
							html = html + '<div class="col-md-3"><input required id="r' + item.periodId +'" class="form-control" type="date" placeholder="提醒时间" ></div>';
						}
						html = html + '<div class="col-md-2"><button type="button"  class="btn btn-primary" onclick="updatePeriod(\''+ item.periodId +'\')">更新</button></div></div>';
						$("#per_row").append(html);
					});			
				}else{
				}
			}
		});
	}
	
	/*合同批量导入*/
	function addContractByExcel(){
		var filePath = $("#excelFile").val();
        if(filePath==null || filePath==""){
			alert("请选择要导入的合同文件，后缀为xls！");
        }else{
            uploadFile();
        }
	};
	
    function uploadFile() {
        $.ajaxFileUpload({
            url: hosturl + '/addContractByExcel',
            secureuri: false,// 一般设置为false
            fileElementId: "excelFile",// 文件上传表单的id <input type="file" id="fileUpload" name="file" />
            dataType: 'json',// 返回值类型 一般设置为json
            data: {},

            success: function (data) // 服务器成功响应处理函数
            {
                var index = parent.layer.getFrameIndex(window.name);
                if(data.status =="empty"){
					alert("请选择要导入的合同文件，后缀为xls！");
                }else if(data.status == "ok"){
                    alert("批量上传成功");
					$('#updateContract').modal('hide');
					setTimeout(function(){window.location.reload(true);},1000);
                }else if(data.status == "error"){
                    alert(data.des);
                }
            }
        });
        return false;
    };
	
	/*添加或更新合同*/
	function savaContracts(type){
		var b = type + "";
		if(b == "add"){
			b = "/addContract";
		}else if(b == "update"){
			b = "/updateContract";
		}
		var contractId = $("#hideId").val();
		var contractSerial = $("#uContractSerial").val();
		var contractName = $("#uContractName").val();
		var contractCate = $("#uContractCate").val() + "";
		var srStartTime = "2000-01-01 00:00:00";
		var srEndTime = "2000-01-01 00:00:00";
		var srRemindTime = "2000-01-01 00:00:00";
		var srIsWarn = 0;
		var eaStartTime = "2000-01-01 00:00:00";
		var eaEndTime = "2000-01-01 00:00:00";
		var eaRemindTime = "2000-01-01 00:00:00";
		var eaIsWarn = 0;
		var ctStartTime = "2000-01-01 00:00:00";
		var ctEndTime = "2000-01-01 00:00:00";
		var ctRemindTime = "2000-01-01 00:00:00";
		var ctIsWarn = 0;
		var ctPeriodNum = 0;
		if(contractCate == "1"){
			srStartTime = $("#uStartTime").val() + " 00:00:00";
			srEndTime = $("#uEndTime").val() + " 00:00:00";
			srIsWarn = $("#uCtIsWarn").is(":checked") ? "1" : "0";
			if(srIsWarn == "1"){
				srRemindTime = $("#uRemindTime").val() + " 00:00:00";
			}
		}else if(contractCate == "2"){
			eaStartTime = $("#uStartTime").val() + " 00:00:00";
			eaEndTime = $("#uEndTime").val() + " 00:00:00";
			eaIsWarn = $("#uCtIsWarn").is(":checked") ? "1" : "0";
			if(eaIsWarn == "1"){
				eaRemindTime = $("#uRemindTime").val() + " 00:00:00";
			}
		}else if(contractCate == "3"){
			ctStartTime = $("#uStartTime").val() + " 00:00:00";
			ctEndTime = $("#uEndTime").val() + " 00:00:00";
			ctIsWarn = $("#uCtIsWarn").is(":checked") ? "1" : "0";
			if(ctIsWarn == "1"){
				ctRemindTime = $("#uRemindTime").val() + " 00:00:00";
			}
			ctPeriodNum = $("#uPeriodNum").val();
		}
		var s = $("#uStartTime").val() + "";
		var e = $("#uEndTime").val() + "";
		var r = $("#uRemindTime").val() + "";
		var f = $("#uCtIsWarn").is(":checked") ? "1" : "0";
		if(s == "" || e == ""){
			alert("开始或结束时间不为空");
			return false;
		}
		if(f == "1"){
			if(r == ""){
				alert("提醒时间不为空");
				return false;	
			}
		}
			var aj = $.ajax({
				url:hosturl + b,
				data:{
					userId:userId,
					deptId:deptId,
					contractId:contractId,
					contractSerial:contractSerial,
					contractName:contractName,
					contractCate:contractCate,
					srStartTime:srStartTime,
					srEndTime:srEndTime,
					srRemindTime:srRemindTime,
					srIsWarn:srIsWarn,
					eaStartTime:eaStartTime,
					eaEndTime:eaEndTime,
					eaIsWarn:eaIsWarn,
					eaRemindTime:eaRemindTime,
					ctStartTime:ctStartTime,
					ctEndTime:ctEndTime,
					ctRemindTime:ctRemindTime,
					ctIsWarn:ctIsWarn,
					ctPeriodNum:ctPeriodNum
				},
				type:"POST",
				dataType:"json",
				success:function(data){
					if(data.status == "ok"){
						alert(data.des);
                    	$('#updateContract').modal('hide');
						setTimeout(function(){window.location.reload(true);},1500);
					}else{
						alert(data.des);
					}
				}
			});
	};
	
	/*设置合同modal框内容*/
	function setContractDataForCtmodal(id){
		$("#myModalLabel2").html("修改合同");
		var aj = $.ajax({
			url:hosturl + '/getContractByID',
			data:{
				contractId:id+""
			},
			type:"GET",
			dataType:"json",
			success:function(data){
				$("#uContractSerial").val(data.data.contractSerial);
				$("#uContractName").val(data.data.contractName);
				$("#hideId").val(data.data.contractId);
				$("#contract_sub").removeAttr("onclick");
				$("#contract_sub").attr("onclick",'savaContracts("update")');			
				var contractCate = data.data.contractCate + "";

				if(contractCate == "1"){
					$("#uStartTime").val(getSmpFormatDateByLong(data.data.srStartTime , false));
					$("#uEndTime").val(getSmpFormatDateByLong(data.data.srEndTime , false));
					$("#uContractCate").html("");
					$("#uContractCate").html('<option value="1" >呈批件</option><option value="2" >审批表</option><option value="3" >合同</option>');
					$("#uContractCate").val('1');
					$("#peridNum_row").hide();
					$("#uPeriodNum").removeAttr("disabled");
					$("#uPeriodNum").val("");
					var warnOrNor = data.data.srIsWarn + "";
					if(warnOrNor == "0"){
						isWarn(warnOrNor);
					}else{
						isWarn(warnOrNor);
						$("#uRemindTime").val(getSmpFormatDateByLong(data.data.srRemindTime , false));
					}
				}else if(contractCate == "2"){
					$("#uStartTime").val(getSmpFormatDateByLong(data.data.eaStartTime , false));
					$("#uEndTime").val(getSmpFormatDateByLong(data.data.eaEndTime , false));
					$("#uContractCate").html("");
					$("#uContractCate").html('<option value="2" >审批表</option><option value="3" >合同</option>');
					$("#uContractCate").val('2');
					$("#peridNum_row").hide();
					$("#uPeriodNum").removeAttr("disabled");
					$("#uPeriodNum").val("");
					var warnOrNor = data.data.eaIsWarn + "";
					if(warnOrNor == "0"){
						isWarn(warnOrNor);

					}else{
						isWarn(warnOrNor);
						$("#uRemindTime").val(getSmpFormatDateByLong(data.data.eaRemindTime ,false));
					}
				}else if(contractCate == "3"){
					$("#uStartTime").val(getSmpFormatDateByLong(data.data.ctStartTime , false));
					$("#uEndTime").val(getSmpFormatDateByLong(data.data.ctEndTime , false));
					$("#uPeriodNum").val(data.data.ctPeriodNum);
					$("#uContractCate").html("");
					$("#uContractCate").html('<option value="3" >合同</option>');	
					$("#uContractCate").val('3');
					$("#peridNum_row").show();
					$("#uPeriodNum").attr("disabled","disabled");
					var warnOrNor = data.data.ctIsWarn + "";
					if(warnOrNor == "0"){
						isWarn(warnOrNor);
					}else{
						isWarn(warnOrNor);
						$("#uRemindTime").val(getSmpFormatDateByLong(data.data.ctRemindTime , false));
					}
				}
			}
		});	
	}
	
	/*部门ID映射为部门名称*/
	function getDeptName(dept , detpId){
		var deptName = "";
		$.each(dept,function(index,item){
//        console.log(item.deptId);
        	if(item.deptId == deptId){
            	deptName = item.deptName;
        	}
    	});
		return deptName;
	};
	
	/*合同是否提醒按钮事件*/
	$("#uCtIsWarn").click(function(){
		 if($("#uCtIsWarn").is(":checked")){
			 $("#reTime_row").show();
			 isWarn("1");
		 }else{
			 $("#reTime_row").hide();
			 isWarn("0");
		 }
	 });
	
	/*更改合同modal框状态*/
	function modifyCtmodalStatus(){
		$("#contract_sub").removeAttr("onclick");
		$("#contract_sub").attr("onclick",'savaContracts("add")');
		$("#uPeriodNum").removeAttr("disabled");
		$("#uPeriodNum").val("");
		$("#peridNum_row").hide();
		$("#myModalLabel2").html("添加合同");		
		$("input.form-control").val("");
		isWarn("1");
		$("#uContractCate").html('<option value="1" >呈批件</option><option value="2" >审批表</option><option value="3" >合同</option>');
	}

		/*合同类型框监听*/
	$("#uContractCate").change(function(){
		var p = $(this).children('option:selected').val();
		$("#uPeriodNum").val("");
		if(p == "3"){
			$("#peridNum_row").show();
		}else{
			$("#peridNum_row").hide();
		}
		isWarn("1");
	});	

	/*合同是否提醒操作*/
	function isWarn(type){
		if(type == "1"){
			$("#uCtIsWarn").removeAttr("checked");
			$("#uCtIsWarn").prop("checked",true);
			$("#reTime_row").show();
		}
		if(type == "0"){
			$("#uCtIsWarn").removeAttr("checked");
			$("#reTime_row").hide();
		}
	}	
    $(document).ready(function(){
        if (userId == null) {
            window.location.href = hosturl + "/login.jsp";
        } else {
            searchContract();
        }
    });
	
	$('#updateContract').on('hide.bs.modal', function () {
  		$("#uCtIsWarn").removeAttr("checked");
		$("#uCtIsWarn").checked="";
	});