<%--
  Created by IntelliJ IDEA.
  User: CMCC-Sunmo
  Date: 2016/9/19
  Time: 10:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>个人信息</title>
    <%--<!--[if !IE]>--%>
    <%--<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-3.1.0.min.js"></script>--%>
    <%--<![endif]-->--%>
    <%--<!--[if gt IE 9]>--%>
    <%--<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-3.1.0.min.js"></script>--%>
    <%--<![endif]-->--%>
    <%--<!--[if lte IE 9]>--%>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.12.2.min.js"></script>
    <%--<![endif]-->--%>

    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
    <!--[if lte IE 9]>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/respond.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/html5shiv.js"></script>
    <![endif]-->
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/md5.js"></script>
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/contract.css" />
</head>
<body>
<%--<jsp:include page="../view/header.jsp" flush="true" />--%>
<jsp:include page="${request.getContextPath() }/view/AdminNav.jsp" flush="true" />
<div class="container mycontainer">
    <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-4">
            <button class="btn btn-primary" data-toggle="modal" data-target="#addAdmin">添加管理员</button>
            <button class="btn btn-primary" data-toggle="modal" data-target="#updateAdminPass">修改密码</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr><th>管理员信息</th><th>内容</th></tr>
                    </thead>
                    <tr>
                        <td>管理员名称</td>
                        <td id="adminName"></td>
                    </tr>
                    <tr>
                        <td>管理员权限</td>
                        <td id="adminRight">管理员</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
<div class="modal fade" id="updateAdminPass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">修改密码</h4>
            </div>
            <div class="modal-body">
                <%--<div class="row">--%>
                <%--<div class="center-block">--%>
                <form class="" role="form">
                    <input id="oldPass" class="form-control" type="text" placeholder="管理员名称">
                    <input id="newPass1" class="form-control" type="password" placeholder="输入密码">
                    <input id="newPass2" class="form-control" type="password" placeholder="确认密码">
                </form>
                <%--</div>--%>
                <%--</div>--%>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="updateAdminPass()">提交</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<div class="modal fade" id="addAdmin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">添加管理员</h4>
            </div>
            <div class="modal-body">
                <%--<div class="row">--%>
                <%--<div class="center-block">--%>
                <form class="" role="form">
                    <input required id="newAdminName" class="form-control" type="text" placeholder="管理员名称">
                    <input required id="newAdminPass1" class="form-control" type="password" placeholder="输入密码">
                    <input required id="newAdminPass2" class="form-control" type="password" placeholder="确认密码">
                </form>
                <%--</div>--%>
                <%--</div>--%>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="newAdmin()">添加</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
</div>
<%--<jsp:include page="../view/footer.jsp" flush="true" />--%>
</body>
</html>
<script>
    var adminId = <%=session.getAttribute("adminId")%>;
    var adminName = "<%=session.getAttribute("adminName")%>";
    $(document).ready(function(){
        $("#adminName").text(adminName);
    });

    function updateAdminPass(){
        adminId = "1";
        var oldPass = $("#oldPass").val();
        var newPass1 = $("#newPass1").val();
        var newPass2 = $("#newPass2").val();
        if(oldPass==""){
            alert("旧密码不能为空");
            $("#oldPass").focus();
            return;
        }
        if(newPass1==""){
            alert("新密码不能为空");
            $("#newPass1").focus();
            return;
        }
        if(newPass2==""){
            alert("确认密码不能为空");
            $("#newPass2").focus();
            return;
        }
        if(newPass1!=newPass2){
            alert("新密码和确认密码不同，请重新输入");
            $("#newPass1").focus();
            return;
        }
        var aj = $.ajax( {
            url:'<%=request.getContextPath() %>/admin/updatePass',
            data:{
                adminId:adminId,
                oldPassword:hex_md5(oldPass),
                newPassword:hex_md5(newPass1)
            },
            type:'POST',
            dataType:'json',
            success:function(data) {
                if(data.status =="ok" ){
                    alert(data.des);
                    $('#updateAdminPass').modal('hide')
                }else{
                    alert(data.des);
                }
            },
            error : function() {
                alert("异常！");
            }
        });
    }
    function newAdmin(){
        var newAdminName = $("#newAdminName").val();
        var newAdminPass1 = $("#newAdminPass1").val();
        var newAdminPass2 = $("#newAdminPass2").val();
        if(newAdminName==""){
            alert("请输入管理员名称！");
            $("#newAdminName").focus();
            return;
        }else{
            var reg = /^[a-zA-Z]\w{0,11}$/;
            if(reg.test(newAdminName)){

            }else{
                alert("管理员名称只能包含字母和数字，首位不能为数字！");
                return;
            }
        }
        if(newAdminPass1==""){
            alert("请输入密码！");
            $("#newAdminPass1").focus();
            return;
        }
        if(newAdminPass2==""){
            alert("请输入确认密码！");
            $("#newAdminPass2").focus();
            return;
        }
        if(newAdminPass1 != newAdminPass2){
            alert("新密码和确认密码不一致！");
            $("#newAdminPass1").focus();
            return;
        }

        var aj = $.ajax( {
            url:'<%=request.getContextPath() %>/admin/addAdmin',
            data:{
                adminName:newAdminName,
                adminPass:hex_md5(newAdminPass1),
            },
            type:'POST',
            dataType:'json',
            success:function(data) {
                if(data.status =="ok" ){
                    alert(data.des);
                    $('#addAdmin').modal('hide')
                }else{
                    alert(data.des);
                    $('#addAdmin').modal('hide')
                }
            },
            error : function() {
                alert("异常！");
            }
        });

    }
</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/common.js"></script>