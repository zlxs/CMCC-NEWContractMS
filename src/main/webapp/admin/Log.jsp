<%--
  Created by IntelliJ IDEA.
  User: CMCC-Sunmo
  Date: 2016/9/22
  Time: 9:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container mycontainer">
    <div class="panel panel-default">
        <div class="panel-heading">搜索条件</div>
        <div class="panel-body">
            <div class="col-md-2">
                <input id="userName" type="text" class="form-control" placeholder="员工编号">
            </div>
            <div class="col-md-2">
                <input id="userIPforLog" type="text" class="form-control" placeholder="用户IP">
            </div>
            <div class="col-md-2">
                <input id="logOperRole" type="text" class="form-control" placeholder="用户权限">
            </div>
            <div class="col-md-2">
                <input id="logOper" type="text" class="form-control" placeholder="用户操作">
            </div>
            <div class="col-md-2">
                <input id="logCTime" type="date" class="form-control" placeholder="日志创建时间">
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary" onclick="searchLog();">搜索</button>
                <label id="dataLen">共有？条</label>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">搜索结果</div>
        <div class="panel-body">
            <div class="row">
                <table id="thead" class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th>日志ID</th>
                        <th>员工编号</th>
                        <th>用户IP</th>
                        <th>用户权限</th>
                        <th>用户操作</th>
                        <th>日志创建时间</th>
                        <%--<th>操作</th>--%>
                    </tr>
                    </thead>
                    <tbody id="data-container">
                    <%--<div id="data-container"></div>--%>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div id="wrapper">
                    <section>
                        <div id="pagination-container"></div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    var adminId = "<%=session.getAttribute("userId")%>";
    var userRight = "<%=session.getAttribute("userRight")%>";
    var dept = <%=session.getAttribute("DepartmentList")%>;
    var deptName = "";
    var deptOption = ""

    $.each(dept,function(index,item){
        deptOption+='<option value="'+item.deptId+'">'+item.deptName+'</option>';
    });
    $("#dept").html($("#dept").html()+deptOption);

    function simpleTemplating(data) {
        var html = '';
        $.each(data, function (index, item) {
            var Role = "";
            switch (item.logOperRole){
                case 1: Role = "总经理";break;
                case 2: Role = "部门经理";break;
                case 3: Role = "员工";break;
                case 0: Role = "管理员";break;
            }
            html += '<tr>';
            html += '<td>'+ item.logId +'</td>';
            html += '<td>'+ item.userName +'</td>';
            html += '<td>'+ item.userIPforLog +'</td>';
            html += '<td>'+ Role +'</td>';
            html += '<td>'+ item.logOper +'</td>';
            html += '<td>'+ getSmpFormatDateByLong(item.logCTime,true) +'</td>';
//            html += '<td><a href="javascript:deleteLog(\'' + item.logId + '\');">删除</a></td>';
            html += '</tr>';
        });
        return html;
    };

    function deleteLog(logId) {
        if(confirm("确定要删除日志吗？")) {
            var aj = $.ajax({
                url: '<%=request.getContextPath() %>/admin/deleteLog',
                data: {
                    logId: logId
                },
                type: 'get',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    alert(data.des);
                    searchLog();
                },
                error: function () {
                    alert("异常！");
                }
            });
        }else{

        }
    };

    function searchLog() {
        var userName = $("#userName").val();
        var userIPforLog = $("#userIPforLog").val();
        var logOperRole = $("#logOperRole").val();
        var logOper = $("#logOper").val();
//        var logCTime = $("#logCTime").val();
//        logCTime = "2000-01-01 00:00:00";
        var logCTime = $("#logCTime").val()==""? "2000-01-01 00:00:00":$("#logCTime").val()+" 00:00:00";
        var aj = $.ajax({
            url:'<%=request.getContextPath() %>/admin/findLog',
            data:{
                userId:userName,
                userIPforLog:userIPforLog,
                logOperRole:logOperRole,
                logOper:logOper,
                logCTime:logCTime
            },
            type:'POST',
            dataType:'json',
            success:function(data) {
                if(data.status =="ok" ){
                    document.getElementById("dataLen").innerHTML="共有"+data.data.length +"条";
                    $('#pagination-container').pagination({
                        dataSource: data.data,
                        pageSize: 20,
                        showGoInput: true,
                        showGoButton: true,
                        autoHidePrevious: true,
                        autoHideNext: true,
                        pageNumber: 1,
                        callback: function (data, pagination) {
                            // template method of yourself
                            var html = simpleTemplating(data);
                            $('#data-container').html(html);
                        }
                    });
                }else{
                    document.getElementById("dataLen").innerHTML="共有0条";
                    $('#data-container').html("");
                    $('#pagination-container').html("");
                }
            },
            error : function() {
                alert("日志数据异常！");
            }
        });

    };

    $(document).ready(function(){
        if (adminId === null) {
            //window.location.href = hosturl+"/login.jsp";
            searchLog();
        } else {
            searchLog();
        }
    });
</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/common.js"></script>
<!--[if lte IE 9]>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/placeholderie8.js"></script>
<![endif]-->