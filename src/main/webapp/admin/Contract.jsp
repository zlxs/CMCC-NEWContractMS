<%--
  Created by IntelliJ IDEA.
  User: CMCC-Sunmo
  Date: 2016/9/19
  Time: 10:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container mycontainer">
    <div class="panel panel-default">
        <div class="panel-heading">搜索条件</div>
        <div class="panel-body">
            <div class="row">
				<div class="col-md-2">
                    <select id="dept" class="form-control">
                    </select>
                </div>
                <div class="col-md-2">
                    <input id="userName" type="text" class="form-control" placeholder="用户名称">
                </div>
                <div class="col-md-2">
                    <input id="contractSer" type="text" class="form-control" placeholder="合同编号">
                </div>
                <div class="col-md-2">
                    <input id="contractName" type="text" class="form-control" placeholder="合同名称">
                </div>
                <div class="col-md-2">
                    <select id="contractCate" class="form-control">
						<option value="0">所有合同</option>
                        <option value="1">呈批件</option>
                        <option value="2">审批表</option>
                        <option value="3">合同</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary" onclick="searchContract();">搜索</button>
                    <label id="dataLen">共有？条</label>
                </div>
            </div>
        </div>
    </div>
	<%--<div class="panel panel-default" >--%>
    <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-4" style="text-align: right;">
		<button onclick="modifyStatus()" class="btn btn-primary" data-toggle="modal" data-target="#updateContract" >添加合同</button>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addConByExcel"  >批量添加合同</button>
        </div>
    </div>
	<%--</div>--%>
    <div class="panel panel-default">
        <div class="panel-heading">搜索结果</div>
        <div class="panel-body">
            <div class="row">
                <table id="thead" class="table table-bordered table-striped table-hover">
                    <colgroup>
                        <col style="">
                        <col style="">
                        <col style="width:150px">
                        <col style="">
                        <col style="width:50px">
                        <col style="width:50px">
                        <col style="">
                        <col style="">
                        <col style="width:50px">
                        <col style="width:100px">
                        <col style="width:100px">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>合同编号</th>
                        <th>合同名称</th>
                        <th>部门</th>
                        <th>用户名</th>
                        <th>合同类型</th>
                        <th>付款期数</th>
                        <th>开始时间</th>
                        <th>结束时间</th>
                        <th>是否提醒</th>
                        <th>提醒时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody id="data-container">
                    <%--<div id="data-container"></div>--%>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div id="wrapper">
                    <section>
                        <div id="pagination-container"></div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="updatePeriod" backdrop="static"  keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
	<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" >修改合同分期</h4>
            </div>
            <div class="modal-body">
				<div class="row">
					<div class="col-md-3">
						<h6 class="modal-title" >付款时间：</h6>
					</div>
					<div class="col-md-2">
						<h6 class="modal-title" >付款金额：</h6>
					</div>
					<div class="col-md-1">
						<h6 class="modal-title" >Tip</h6>
					</div>
					<div class="col-md-3">
						<h6 class="modal-title" >提醒时间：</h6>
					</div>
					<div class="col-md-2">
						<h6 class="modal-title" >操作</h6>
					</div>
				</div>
				<div  id="per_row">
<!--
					<div class="col-md-3"><input required id="" class="form-control" type="date" placeholder="付款时间"></div>
					<div class="col-md-3"><input required id="" class="form-control" type="text" placeholder="付款金额"></div>
					<div class="col-md-3"><input required id="" class="form-control" type="date" placeholder="提醒时间"></div>
					<div class="col-md-3"><button type="button" id="per_sub" class="btn btn-primary" onclick="updatePeriod()">提交</button></div>
-->
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="addConByExcel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel1">批量上传合同</h4>
            </div>
            <div class="modal-body">
				<input required id="excelFile" class="btn" type="file"  name="file" size="15" accept=".xls" placeholder="上传Excel文件" enctype="multipart/form-data" maxlength="100">
			</div>
			<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" id="excel_sub" class="btn btn-primary" onclick="addContractByExcel()">提交</button>
            </div>
		</div>
	</div>
</div>
<div class="modal fade" id="updateContract" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel2">修改合同</h4>
            </div>
            <div class="modal-body">
                <%--<div class="row">--%>
                    <%--<div class="center-block">--%>
                <form class="" role="form">
					合同类型：<select id="uContractCate" class="form-control">
                        <option value="1" >呈批件</option>
                        <option value="2" >审批表</option>
                        <option value="3" >合同</option>
                    </select>
					<input type="hidden" id="hideId"/>
                    合同编号：<input required id="uContractSerial"  class="form-control" type="text" placeholder="输入合同编号">
                    合同名称：<input required id="uContractName" class="form-control" type="text" placeholder="输入合同名称">
					提交人：<input required id="uUserId" class="form-control" type="text" placeholder="输入合同提交人">
					<div id="sr">
						起始时间：<input required id="uSrStartTime" onfocus="calendar()" class="form-control" placeholder="输入呈批件开始时间">
						结束时间：<input required id="uSrEndTime" onfocus="calendar()" class="form-control" placeholder="输入呈批件结束时间">
						是否提醒：<input id="uSrIsWarn" type="checkbox" checked="checked"/><br/>
						<div id="sr_remind">
						提醒时间：<input required id="uSrRemindTime" onfocus="calendar()" class="form-control" placeholder="输入呈批件提醒时间">
						</div>
					</div>
					<div id="ea">
						起始时间：<input required id="uEaStartTime" onfocus="calendar()" class="form-control" placeholder="输入审批表开始时间">
						结束时间：<input required id="uEaEndTime" onfocus="calendar()" class="form-control" placeholder="输入审批表结束时间">
						是否提醒：<input id="uEaIsWarn" type="checkbox" checked="checked"/><br/>
						<div id="ea_remind">
						提醒时间：<input required id="uEaRemindTime" onfocus="calendar()" class="form-control" placeholder="输入审批表提醒时间">
						</div>
					</div>
					<div id="ct">
						起始时间：<input required id="uCtStartTime" onfocus="calendar()" class="form-control" placeholder="输入合同开始时间">
						结束时间：<input required id="uCtEndTime" onfocus="calendar()" class="form-control" placeholder="输入合同结束时间">
						分期次数：<input required id="uCtPeriodNum" class="form-control" type="text" placeholder="输入合同分期次数">
						是否提醒：<input id="uCtIsWarn" type="checkbox" checked="checked"/><br/>
						<div id="ct_remind">
						提醒时间：<input required id="uCtRemindTime" onfocus="calendar()" class="form-control" placeholder="输入合同提醒时间">
						</div>
					</div>
                </form>
                    <%--</div>--%>
                <%--</div>--%>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" id="sub" class="btn btn-primary" onclick="addOrUpdateContract()">提交</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<script>
    var userId = "<%=session.getAttribute("adminId")%>";
    var adminFallName = "<%=session.getAttribute("adminFallName")%>";
	var adminName = "<%=session.getAttribute("adminName")%>";
    var deptId = "<%=session.getAttribute("deptId")%>";
    var userRight = "<%=session.getAttribute("userRight")%>";
    var dept = <%=session.getAttribute("DepartmentList")%>;
    var deptName = "";
    var deptOption = '<option value="0">所有部门</option>';
	var contracts;
	
	
    $.each(dept,function(index,item){
        deptOption+='<option value="'+item.deptId+'">'+item.deptName+'</option>';
    });
    $("#dept").html($("#dept").html()+deptOption);

    function simpleTemplating(data) {
        var html = '';
        $.each(data, function (index, item) {
			var contractCate = item.contractCate + "";
			var deptName = getDeptName(dept , item.deptId);
			html += '<tr>';
            html += '<td>'+ item.contractSerial +'</td>';
			html += '<td>'+ item.contractName +'</td>';
			html += '<td>'+ item.deptName +'</td>';
			html += '<td>'+ item.userName +'</td>';
			if(contractCate == "1"){
				var warnOrNor = (item.srIsWarn == "0")?"否":"是";
				var remindTime = (item.srRemindTime == null)?"":item.srRemindTime;
				html += '<td>'+ "呈批件" +'</td>';
				html += '<td>'+ "无" +'</td>';
				html += '<td>'+ getSmpFormatDateByLong(item.srStartTime , false) +'</td>';
				html += '<td>'+ getSmpFormatDateByLong(item.srEndTime , false) +'</td>';
				html += '<td>'+ warnOrNor +'</td>';
				if(warnOrNor == "是"){
					html += '<td>'+ getSmpFormatDateByLong(remindTime , false) +'</td>'	
				}else{
					html += '<td></td>'
				}
			}else if(contractCate == "2"){
				var warnOrNor = (item.eaIsWarn == "0")?"否":"是";
				var remindTime = (item.eaRemindTime == null)?"":item.eaRemindTime;
				html += '<td>'+ "审批表" +'</td>';
				html += '<td>'+ "无" +'</td>';
				html += '<td>'+ getSmpFormatDateByLong(item.eaStartTime , false) +'</td>';
				html += '<td>'+ getSmpFormatDateByLong(item.eaEndTime , false) +'</td>';
				html += '<td>'+ warnOrNor +'</td>';
				if(warnOrNor == "是"){
					html += '<td>'+ getSmpFormatDateByLong(remindTime , false) +'</td>'	
				}else{
					html += '<td></td>'
				}
			}else if(contractCate == "3"){
				var warnOrNor = (item.ctIsWarn == "0")?"否":"是";
				var remindTime = (item.ctRemindTime == null)?"":item.ctRemindTime;
				html += '<td>'+ "合同" +'</td>';
				html += '<td><a data-toggle="modal" data-target="#updatePeriod" onclick="setPeriod(\''+ item.contractId +'\','+ item.ctPeriodNum +')">'+ item.ctPeriodNum +'</a></td>';
				html += '<td>'+ getSmpFormatDateByLong(item.ctStartTime , false) +'</td>';
				html += '<td>'+ getSmpFormatDateByLong(item.ctEndTime ,false) +'</td>';
				html += '<td>'+ warnOrNor +'</td>';
				if(warnOrNor == "是"){
					html += '<td>'+ getSmpFormatDateByLong(remindTime , false) +'</td>'	
				}else{
					html += '<td></td>'
				}
			}
            html +=  '<td><a onclick="setContract(\''+ item.contractId +'\')" data-toggle="modal" data-target="#updateContract">修改</a>&nbsp&nbsp&nbsp';
			html +=  '<a onclick="delContract(\''+ item.contractId +'\')" >删除</a></td>';
            html += '</tr>';
        });
        return html;
    };
	
//	function addContract(){
//		$("#sub").removeAttr("onclick");
//		$("#sub").attr("onclick","updateContract()");
//	};
	
	function updatePeriod(id){
		var PaidTime = document.getElementById("p" + id).value + " 00:00:00";
		var periodMoney = document.getElementById("m" + id).value + "";
		var periodRemindTime = "2000-01-01 00:00:00";
		var isWarn = "0";
		if($("#c"+id).is(":checked")){
			isWarn = "1";
			periodRemindTime = document.getElementById("r" + id).value + " 00:00:00"
		}
		var aj = $.ajax({
			url:'<%=request.getContextPath() %>/admin/updatePeriod',
			data:{
				periodId:id + "",
				periodIsWarn:isWarn,
				periodPaidTime:PaidTime,
				periodMoney:periodMoney,
				periodIsWarn:isWarn,
				periodRemindTime:periodRemindTime
			},
			type:"POST",
			dataType:"json",
			success:function(data){
				if(data.status == "ok"){
					alert(data.des);
				}else{
					alert(data.des);
				}
			},
			error:function(){
				alert("合同分期 请正确填写时间！");
			}
		});
	}
	
	function setPeriod(id , num){
//		$('#updatePeriod').modal({backdrop: 'static', keyboard: false});
		$("#per_row").html("");
		var aj = $.ajax({
			url:'<%=request.getContextPath() %>/admin/getPeriodByCID',
			data:{
				contractId:id+""
			},
			type:"GET",
			dataType:"json",
			success:function(data){
				if(data.status == "ok"){
						$.each(data.data, function (index, item) {
						var html = '<div class="row"><div class="col-md-3"><input required id="p'+ item.periodId +'" class="form-control " type="date" placeholder="付款时间" value="'+ getSmpFormatDateByLong(item.periodPaidTime , false) +'" ></div>';
						html = html + '<div class="col-md-2"><input required id="m' +    item.periodId +'" class="form-control" type="text" placeholder="付款金额" value="' + item.periodMoney + '" ></div>';
						var isWarn = item.periodIsWarn + "";
						if(isWarn == "1"){
							html = html + '<div class="col-md-1"><input required id="c' + item.periodId +'" class="form-control" type="checkbox" checked="checked"></div>';
							html = html + '<div class="col-md-3"><input required id="r' + item.periodId +'" class="form-control" type="date" placeholder="提醒时间" value="' + getSmpFormatDateByLong(item.periodRemindTime , false) + '" ></div>';
						}else{
							html = html + '<div class="col-md-1"><input required id="c' + item.periodId +'" class="form-control" type="checkbox" ></div>';
							html = html + '<div class="col-md-3"><input required id="r' + item.periodId +'" onfocus="calendar()" class="form-control" placeholder="提醒时间" ></div>';
						}
						html = html + '<div class="col-md-2"><button type="button"  class="btn btn-primary" onclick="updatePeriod(\''+ item.periodId +'\')">更新</button></div></div>';
						$("#per_row").append(html);
					});			
				}else{
//					for(var i=0; i<num; i++){
//						var html = '<div class="row"><div class="col-md-4"><input required id="p'+ i +'" class="form-control" type="date" placeholder="付款时间" value=""></div>';
//						html = html + '<div class="col-md-2"><input required id="m' + i +'" class="form-control" type="text" placeholder="付款金额" value="0"></div>';
//						html = html + '<div class="col-md-4"><input required id="r' + i +'" class="form-control" type="date" placeholder="提醒时间" value=""></div>';
//						html = html + '<div class="col-md-2"><button type="button"  class="btn btn-primary" onclick="updatePeriod(\''+ i +'\',\''+ id +'\')">提交</button></div></div>';
//						$("#per_row").append(html);
//					}
				}
			},
			error:function(){
				alert("异常");
			}
		});
	}
	
	function addContractByExcel(){
		var filePath = $("#excelFile").val();
        if(filePath==null || filePath==""){
			alert("请选择要导入的合同文件，后缀为xls！");
        }else{
            uploadFile();
        }
	};
	
    function uploadFile() {
        $.ajaxFileUpload({
            url: '<%=request.getContextPath() %>/admin/addContractByExcel',
            secureuri: false,// 一般设置为false
            fileElementId: "excelFile",// 文件上传表单的id <input type="file" id="fileUpload" name="file" />
            dataType: 'json',// 返回值类型 一般设置为json
            data: {},

            success: function (data) // 服务器成功响应处理函数
            {
                if(data.status =="empty"){
					alert("请选择要导入的合同文件，后缀为xls！");
                }else if(data.status == "ok"){
                    alert("批量上传成功");
					$('#updateContract').modal('hide');
                }else if(data.status == "error"){
                    alert(data.des);
                }
            },
            error: function (xhr, type, errorThrown)// 服务器响应失败处理函数
            {
                console.log(type);
            }
        });
        return false;
    };
	
	function addOrUpdateContract(flag){
		var b = flag + "";
		var contractId = $("#hideId").val();
		var contractSerial = $("#uContractSerial").val();
		var contractName = $("#uContractName").val();
		var contractCate = $("#uContractCate").val() + "";
		var userId = $("#uUserId").val();
		var srStartTime = "2000-01-01 00:00:00";
		var srEndTime = "2000-01-01 00:00:00";
		var srRemindTime = "2000-01-01 00:00:00";
		var srIsWarn = 0;
		var eaStartTime = "2000-01-01 00:00:00";
		var eaEndTime = "2000-01-01 00:00:00";
		var eaRemindTime = "2000-01-01 00:00:00";
		var eaIsWarn = 0;
		var ctStartTime = "2000-01-01 00:00:00";
		var ctEndTime = "2000-01-01 00:00:00";
		var ctRemindTime = "2000-01-01 00:00:00";
		var ctIsWarn = 0;
		var ctPeriodNum = 0;
		if(contractCate == "1"){
			srStartTime = $("#uSrStartTime").val() + " 00:00:00";
			srEndTime = $("#uSrEndTime").val() + " 00:00:00";
			srIsWarn = $("#uSrIsWarn").is(":checked") ? "1" : "0";
			if(srIsWarn == "1"){
				srRemindTime = $("#uSrRemindTime").val() + " 00:00:00";
			}
		}else if(contractCate == "2"){
			eaStartTime = $("#uEaStartTime").val() + " 00:00:00";
			eaEndTime = $("#uEaEndTime").val() + " 00:00:00";
			eaIsWarn = $("#uEaIsWarn").is(":checked") ? "1" : "0";
			if(eaIsWarn == "1"){
				eaRemindTime = $("#uEaRemindTime").val() + " 00:00:00";
			}
		}else if(contractCate == "3"){
			ctStartTime = $("#uCtStartTime").val() + " 00:00:00";
			ctEndTime = $("#uCtEndTime").val() + " 00:00:00";
			ctIsWarn = $("#uCtIsWarn").is(":checked") ? "1" : "0";
			if(ctIsWarn == "1"){
				ctRemindTime = $("#uCtRemindTime").val() + " 00:00:00";
			}
			ctPeriodNum = $("#uCtPeriodNum").val();
		}
		if(b == "add"){
			var aj = $.ajax({
				url:'<%=request.getContextPath() %>/admin/addContract',
				data:{
					userId:userId,
					deptId:deptId,
					contractId:contractId,
					contractSerial:contractSerial,
					contractName:contractName,
					contractCate:contractCate,
					srStartTime:srStartTime,
					srEndTime:srEndTime,
					srRemindTime:srRemindTime,
					srIsWarn:srIsWarn,
					eaStartTime:eaStartTime,
					eaEndTime:eaEndTime,
					eaIsWarn:eaIsWarn,
					eaRemindTime:eaRemindTime,
					ctStartTime:ctStartTime,
					ctEndTime:ctEndTime,
					ctRemindTime:ctRemindTime,
					ctIsWarn:ctIsWarn,
					ctPeriodNum:ctPeriodNum
				},
				type:"POST",
				dataType:"json",
				success:function(data){
					if(data.status == "ok"){
						alert(data.des);
                    	$('#updateContract').modal('hide');
					}else{
						alert(data.des);
					}
				},
				error:function(){
					alert("呈批件/审批表/合同 请正确填写时间！");
				}
			});
			$("#sub").removeAttr("onclick");
			$("#sub").attr("onclick",'addOrUpdateContract("add")');
		}else if(b == "update"){
			var aj = $.ajax({
				url:'<%=request.getContextPath() %>/admin/updateContract',
				data:{
					userId:userId,
					contractId:contractId,
					contractSerial:contractSerial,
					contractName:contractName,
					contractCate:contractCate,
					srStartTime:srStartTime,
					srEndTime:srEndTime,
					srRemindTime:srRemindTime,
					srIsWarn:srIsWarn,
					eaStartTime:eaStartTime,
					eaEndTime:eaEndTime,
					eaIsWarn:eaIsWarn,
					eaRemindTime:eaRemindTime,
					ctStartTime:ctStartTime,
					ctEndTime:ctEndTime,
					ctRemindTime:ctRemindTime,
					ctIsWarn:ctIsWarn,
					ctPeriodNum:ctPeriodNum
				},
				type:"POST",
				dataType:"json",
				success:function(data){
					if(data.status == "ok"){
						alert(data.des);
                    	$('#updateContract').modal('hide');
					}else{
						alert(data.des);
					}
				},
				error:function(){
					alert("呈批件/审批表/合同 请正确填写时间！");
				}
			});
		}
		setTimeout(function(){window.location.reload(true);},1000);
	};
	
	function delContract(id){
		if (confirm("确定要删除合同吗？")){
			var aj = $.ajax({
				url:"<%=request.getContextPath() %>/admin/deleteContract",
				data:{
					ContractId: id + ""
				},
				type:"GET",
				dataType:"json",
				success:function(data){
					if(data.status == "ok"){
						alert(data.des);	
						setTimeout(function(){window.location.reload(true);},1000);
					}else{
						alert(data.des);		
					}
				}
			});
		};
	}
	
	function setContract(id){
		$("#myModalLabel2").html("修改合同");
//		$("#uUserId").attr("disabled","disabled");
		var aj = $.ajax({
			url:'<%=request.getContextPath() %>/admin/getContractByID',
			data:{
				contractId:id+""
			},
			type:"GET",
			dataType:"json",
			success:function(data){
				$("#uContractSerial").val(data.data.contractSerial);
				$("#uContractName").val(data.data.contractName);
				$("#hideId").val(data.data.contractId);
				$("#uUserId").val(data.data.userId);
				$("#sub").removeAttr("onclick");
				$("#sub").attr("onclick",'addOrUpdateContract("update")');
				var contractCate = data.data.contractCate + "";

				if(contractCate == "1"){
					$("#uCtPeriodNum").removeAttr("disabled");
					$("#uSrStartTime").val(getSmpFormatDateByLong(data.data.srStartTime , false));
					$("#uSrEndTime").val(getSmpFormatDateByLong(data.data.srEndTime , false));
					$("#uSrRemindTime").val(getSmpFormatDateByLong(data.data.srRemindTime , false));
					$("#uContractCate").html("");
					$("#uContractCate").html('<option value="1" >呈批件</option><option value="2" >审批表</option><option value="3" >合同</option>');
					$("#uContractCate").val('1');
					var warnOrNor = data.data.srIsWarn + "";
					if(warnOrNor == "0"){
//                        $("#uSrIsWarn").checked = "";
//						$("#uSrIsWarn").checked = false;
                        $("#uSrIsWarn").removeAttr("checked");
						$("#sr_remind").hide();
					}else{
						$("#uSrIsWarn").attr("checked","true");
						$("#sr_remind").show();
					}
					$("#sr").show();
					$("#ea").hide();
					$("#ct").hide();
				}else if(contractCate == "2"){
					$("#uCtPeriodNum").removeAttr("disabled");
					$("#uEaStartTime").val(getSmpFormatDateByLong(data.data.eaStartTime , false));
					$("#uEaEndTime").val(getSmpFormatDateByLong(data.data.eaEndTime , false));
					$("#uEaRemindTime").val(getSmpFormatDateByLong(data.data.eaRemindTime ,false));
					$("#uContractCate").html("");
					$("#uContractCate").html('<option value="2" >审批表</option><option value="3" >合同</option>');
					$("#uContractCate").val('2');
					var warnOrNor = data.data.eaIsWarn + "";
					if(warnOrNor == "0"){
						$("#uEaIsWarn").removeAttr("checked");
						$("#ea_remind").hide();
					}else{
						$("#uEaIsWarn").attr("checked","checked");
						$("#ea_remind").show();
					}
					$("#sr").hide();
					$("#ea").show();
					$("#ct").hide();
				}else if(contractCate == "3"){
					$("#uCtStartTime").val(getSmpFormatDateByLong(data.data.ctStartTime , false));
					$("#uCtEndTime").val(getSmpFormatDateByLong(data.data.ctEndTime , false));
					$("#uCtRemindTime").val(getSmpFormatDateByLong(data.data.ctRemindTime , false));
					$("#uCtPeriodNum").val(data.data.ctPeriodNum);
					$("#uCtPeriodNum").attr("disabled", "disabled");
					$("#uContractCate").html("");
					$("#uContractCate").html('<option value="3" >合同</option>');	
					$("#uContractCate").val('3');
					var warnOrNor = data.data.ctIsWarn + "";
					if(warnOrNor == "0"){
						$("#uCtIsWarn").removeAttr("checked");
						$("#ct_remind").hide();
					}else{
						$("#uCtIsWarn").attr("checked","checked");
                        $("#uctiswarn").flushInline();
						$("#ct_remind").show();
					}
					$("#sr").hide();
					$("#ea").hide();
					$("#ct").show();
				}
			},
			error:function(){
				alert("异常");
			}
		});	
	}
	
	function getDeptName(dept , detpId){
		var deptName = "";
		$.each(dept,function(index,item){
//        console.log(item.deptId);
        	if(item.deptId == deptId){
            	deptName = item.deptName;
        	}
    	});
		return deptName;
	};

    function searchContract() {
        var userName = $("#userName").val();
        var contractSer = $("#contractSer").val();
        var contractName = $("#contractName").val();
        var contractCate = $("#contractCate").val();
		var deptId = $("#dept").val();
        var aj = $.ajax( {
            url:'<%=request.getContextPath() %>/admin/findContract',
            data:{
                deptId:deptId,
                userId:userName,
                contractSerial:contractSer,
                contractName:contractName,
                contractCate:contractCate
            },
            type:'POST',
            dataType:'json',
            success:function(data) {
				contracts = data;
                if(data.status =="ok" ){
                    document.getElementById("dataLen").innerHTML="共有"+data.data.length +"条";
					$('#data-container').html("");
					$('#pagination-container').html("");
                    $('#pagination-container').pagination({
                        dataSource: data.data,
                        pageSize: 20,
                        showGoInput: true,
                        showGoButton: true,
                        autoHidePrevious: true,
                        autoHideNext: true,
//                        showPrevious: false,
//                        showNext: false,
//                        showPageNumbers: false,
//                        showNavigator: true,
                        pageNumber: 1,
                        callback: function (data, pagination) {
                            // template method of yourself
                            var html = simpleTemplating(data);
                            $('#data-container').html(html);
                        }
                    });
                }else{
					document.getElementById("dataLen").innerHTML="共有0条";
					$('#data-container').html("");
					$('#pagination-container').html("");
                }
            },
            error : function() {
                alert("异常！");
            }
        });
    }
	
	 $("#uSrIsWarn").click(function(){
		 if($("#uSrIsWarn").is(":checked")){
			 $("#sr_remind").show();
		 }else{
			 $("#sr_remind").hide();
		 }
	 });
	
	$("#uEaIsWarn").click(function(){
		 if($("#uEaIsWarn").is(":checked")){
			 $("#ea_remind").show();
		 }else{
			 $("#ea_remind").hide();
		 }
	 });
	
	$("#uCtIsWarn").click(function(){
		 if($("#uCtIsWarn").is(":checked")){
			 $("#ct_remind").show();
		 }else{
			 $("#ct_remind").hide();
		 }
	 });
	
	function modifyStatus(){
		$("#sub").removeAttr("onclick");
		$("#sub").attr("onclick",'addOrUpdateContract("add")');
		$("#uCtPeriodNum").removeAttr("disabled");
		$("#uUserId").show();
		$("#myModalLabel2").html("添加合同");		
		$("input.form-control").val("");
		$("#sr").show();
		$("#ea").hide();
		$("#ct").hide();
        $("#uSrIsWarn").prop("checked",true);
        $("#uEaIsWarn").prop("checked",true);
        $("#uCtIsWarn").prop("checked",true);

		$("#uContractCate").html("");
		$("#uContractCate").html('<option value="1" >呈批件</option><option value="2" >审批表</option><option value="3" >合同</option>');
	}
	
	
    $(document).ready(function(){
        if (userId == null) {
            window.location.href = "<%=request.getContextPath() %>/login.jsp";
        } else {
            searchContract();
        }
		$("#uContractCate").change(function(){
			var p = $(this).children('option:selected').val();
			if(p == "1"){
				$("#sr").show();
				$("#ea").hide();
				$("#ct").hide();
                $("#uSrIsWarn").attr("checked",true);
			}else if(p == "2"){
				$("#sr").hide();
				$("#ea").show();
				$("#ct").hide();
                $("#uEaIsWarn").attr("checked",true);
			}else if(p == "3"){
				$("#sr").hide();
				$("#ea").hide();
				$("#ct").show();
                $("#uCtIsWarn").attr("checked",true);
			}else{
				$("#sr").hide();
				$("#ea").hide();
				$("#ct").hide();
			}
		});
    });
	
	$('#updateContract').on('hide.bs.modal', function () {
  		$("#uSrIsWarn").removeAttr("checked");
		$("#uSrIsWarn").checked="";
	});
</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/common.js"></script>
<!--[if lte IE 9]>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/placeholderie8.js"></script>
<![endif]-->
<%--<script type="text/javascript" src="<%=request.getContextPath() %>/js/lib/contract.js"></script>--%>