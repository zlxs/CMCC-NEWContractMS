<%--
  Created by IntelliJ IDEA.
  User: CMCC-Sunmo
  Date: 2016/9/19
  Time: 10:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container mycontainer">
    <div class="panel panel-default">
        <div class="panel-heading">搜索条件</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-2">
                    <select id="dept" class="form-control">
                        <option value="0">所有员工</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <input id="userFullName" type="text" class="form-control" placeholder="用户姓名">
                </div>
                <div class="col-md-2">
                    <input id="userName" type="text" class="form-control" placeholder="员工编号">
                </div>
                <div class="col-md-2">
                    <input id="userRight" type="text" class="form-control" placeholder="用户权限">
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary" onclick="searchUser();">搜索</button>
                    <label id="dataLen">共有？条</label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-4" style="text-align: right;">
            <button class="btn btn-primary" data-toggle="modal" data-target="#addUser"  >添加用户</button>
            <button class="btn btn-primary" data-toggle="modal" data-target="#addUserByExcel"  >批量添加用户</button>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">搜索结果</div>
        <div class="panel-body">
            <div class="row">
                <table id="thead" class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th>员工编号</th>
                        <th>部门</th>
                        <th>用户OA邮箱</th>
                        <th>用户普通邮箱</th>
                        <th>用户姓名</th>
                        <th>用户手机</th>
                        <th>用户权限</th>
                        <%--<th>用户加密盐值</th>--%>
                        <%--<th>用户状态</th>--%>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody id="data-container">
                    <%--<div id="data-container"></div>--%>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div id="wrapper">
                    <section>
                        <div id="pagination-container"></div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="updateUser" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">修改用户信息</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="hideId"/>
                用户ID：<input required id="uUserId"  class="form-control" type="text" readonly="readonly">
                员工编号：<input required id="uUserName"  class="form-control" type="text" placeholder="输入员工编号">
                部门：<input required id="uDepID" class="form-control" type="text" placeholder="输入部门编号">
                OA邮箱：<input required id="uUserOaEmail" class="form-control" type="text" placeholder="输入OA邮箱">
                普通邮箱：<input required id="uUserOrdinaryEmail" class="form-control" type="text" placeholder="输入普通邮箱">
                姓名：<input required id="uUserFullName" class="form-control" type="text" placeholder="输入姓名">
                手机：<input required id="uUserPhone" class="form-control" type="text" placeholder="输入手机号码">
                权限：<input required id="uUserRight" class="form-control" type="text" placeholder="输入用户权限">
                状态：<input required id="uUserStatus" class="form-control" type="text" placeholder="输入用户状态">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="updateUser()">提交</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="addUserModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="addModalLabel">添加用户</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="ahideId"/>
                员工编号：<input required id="aUserName"  class="form-control" type="text" placeholder="输入员工编号">
                部门：<input required id="aDepID" class="form-control" type="text" placeholder="输入部门编号">
                OA邮箱：<input required id="aUserOaEmail" class="form-control" type="text" placeholder="输入OA邮箱">
                普通邮箱：<input required id="aUserOrdinaryEmail" class="form-control" type="text" placeholder="输入普通邮箱">
                姓名：<input required id="aUserFullName" class="form-control" type="text" placeholder="输入姓名">
                手机：<input required id="aUserPhone" class="form-control" type="text" placeholder="输入手机号码">
                权限：<select id="aUserRight" class="form-control">
                    <option value="1" >总经理</option>
                    <option value="2" >部门经理</option>
                    <option value="3" >员工</option>
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="addUser()">提交</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<div class="modal fade" id="addUserByExcel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel1">批量添加用户</h4>
            </div>
            <div class="modal-body">
                <input required id="excelFile" class="btn" type="file"  name="file" size="15" accept=".xls" placeholder="上传Excel文件" enctype="multipart/form-data" maxlength="100">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" id="excel_sub" class="btn btn-primary" onclick="addUserByExcel()">提交</button>
            </div>
        </div>
    </div>
</div>
<script>

    var adminId = "<%=session.getAttribute("userId")%>";
    var userRight = "<%=session.getAttribute("userRight")%>";
    var dept = <%=session.getAttribute("DepartmentList")%>;
    var deptName = "";
    var deptOption = ""

    $.each(dept,function(index,item){
        deptOption+='<option value="'+item.deptId+'">'+item.deptName+'</option>';
    });
    $("#dept").html($("#dept").html()+deptOption);

    function simpleTemplating(data) {
        var html = '';

        $.each(data, function (index, item) {
            var deptName = "";
            $.each(dept,function(inddex1,item1){
                if(item1.deptId == item.deptId){
                    deptName = item1.deptName;
                }
            });
            var userQuality = "";
            switch (item.userRight){
                case 1: userQuality = "总经理";break;
                case 2: userQuality = "部门经理";break;
                case 3: userQuality = "员工";break;
            }
            html += '<tr>';
            html += '<td>'+ item.userName +'</td>';
            html += '<td>'+ deptName +'</td>';
            html += '<td>'+ item.userOaEmail +'</td>';
            html += '<td>'+ item.userOrdinaryEmail +'</td>';
            html += '<td>'+ item.userFullName +'</td>';
            html += '<td>'+ item.userPhone +'</td>';
            html += '<td>'+ userQuality +'</td>';
//            html += '<td>'+ item.userPrimarySalt +'</td>';
//            html += '<td>'+ item.userStatus +'</td>';
            html += '<td><a href="javascript:updatePass(\'' + item.userId + '\');">重置密码</a>&nbsp;&nbsp;&nbsp;<a  data-toggle="modal" data-target="#updateUser" onclick=" update(\'' + item.userId + '\');">修改</a>&nbsp;&nbsp;&nbsp;<a href="javascript:deleteUser(\'' + item.userId + '\');">删除</a></td>';
            html += '</tr>';
        });
        return html;
    };

    function deleteUser(userId) {
        if(confirm("确定要删除用户吗？")) {
            var aj = $.ajax({
                url: '<%=request.getContextPath() %>/admin/deleteUser',
                data: {
                    userId: userId
                },
                type: 'post',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    alert(data.des);
                    searchUser();
                },
                error: function () {
                    alert("异常！");
                }
            });
        }else{

        }
    };

    function updatePass(userId) {
        if(confirm("确定要重置用户密码吗？")) {
            var aj = $.ajax({
                url: '<%=request.getContextPath() %>/admin/resetUserPass',
                data: {
                    userId: userId,
                    newPass: hex_md5("123456")
                },
                type: 'post',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    alert(data.des);
                    searchUser();
                },
                error: function () {
                    alert("异常！");
                }
            });
        }else{

        }
    };

    function searchUser() {
        var userFullName = $("#userFullName").val();
        var userName = $("#userName").val();
        var deptId = $("#dept").val();
        var userRight = $("#userRight").val();

        var aj = $.ajax({
            url:'<%=request.getContextPath() %>/admin/findUser',
            data:{
                userFullName:userFullName,
                userName:userName,
                deptId:deptId,
                userRight:userRight,
            },
            type:'POST',
            dataType:'json',
            success:function(data) {
                if(data.status =="ok" ){
                    document.getElementById("dataLen").innerHTML="共有"+data.data.length +"条";
                    $('#pagination-container').pagination({
                        dataSource: data.data,
                        pageSize: 20,
                        showGoInput: true,
                        showGoButton: true,
                        autoHidePrevious: true,
                        autoHideNext: true,
                        pageNumber: 1,
                        callback: function (data, pagination) {
                            // template method of yourself
                            var html = simpleTemplating(data);
                            $('#data-container').html(html);
                        }
                    });
                }else{
                    document.getElementById("dataLen").innerHTML="共有0条";
                    $('#data-container').html("");
                    $('#pagination-container').html("");
                }
            },
            error : function() {
                alert("用户数据异常！");
            }
        });

    };

    function update(userId) {
        $("#userModalLabel").html("修改用户");

        var aj = $.ajax({
            url:'<%=request.getContextPath() %>/admin/getUserfInfo',
            data:{
                userId:userId+""
            },
            type:"GET",
            dataType:"json",
            success:function(data){

                $("#uUserId").val(data.data.userId);
                $("#uUserName").val(data.data.userName);
                $("#uDepID").val(data.data.deptId);
                $("#uUserOaEmail").val(data.data.userOaEmail);
                $("#uUserOrdinaryEmail").val(data.data.userOrdinaryEmail);
                $("#uUserFullName").val(data.data.userFullName);
                $("#uUserPhone").val(data.data.userPhone);
                $("#uUserRight").val(data.data.userRight);
                $("#uUserStatus").val(data.data.userStatus);

            },
            error:function(){
                alert("异常");
            }
        });
    };

    function updateUser() {
        var userId = $("#uUserId").val();
        var userName = $("#uUserName").val();
        var deptId = $("#uDepID").val();
        var userOaEmail = $("#uUserOaEmail").val();
        var userOrdinaryEmail = $("#uUserOrdinaryEmail").val();
        var userFullName = $("#uUserFullName").val();
        var userPhone = $("#uUserPhone").val();
        var userRight = $("#uUserRight").val();
        var userStatus = $("#uUserStatus").val();

        var aj = $.ajax({
            url:'<%=request.getContextPath() %>/admin/updateUserInfo',
            data:{
                userId:userId,
                userName:userName,
                deptId:deptId,
                userOaEmail:userOaEmail,
                userOrdinaryEmail:userOrdinaryEmail,
                userFullName:userFullName,
                userPhone:userPhone,
                userRight:userRight,
                userStatus:userStatus
            },
            type:"POST",
            dataType:"json",

            success:function(data){
                if(data.status == "ok"){
                    alert(data.des);
                    $('#updateUser').modal('hide');
                    searchUser();
                }
            },
            error:function(){

            }
        })
    };

    function addUserByExcel(){
        var filePath = $("#excelFile").val();
        if(filePath==null || filePath==""){
            alert("请选择要导入的用户文件，后缀为xls！");
        }else{
            uploadFile();
        }
    };

    function add() {
        $("#addUserModalLabel").html("添加用户");
    }

    function addUser() {
        var userName = $("#aUserName").val();
        var userPass = hex_md5("123456");
        var deptId = $("#aDepID").val();
        var userOaEmail = $("#aUserOaEmail").val();
        var userOrdinaryEmail = $("#aUserOrdinaryEmail").val();
        var userFullName = $("#aUserFullName").val();
        var userPhone = $("#aUserPhone").val();
        var userRight = $("#aUserRight").val();

        var aj = $.ajax({
            url:'<%=request.getContextPath() %>/admin/addUser',
            data:{
                userName:userName,
                userPass:userPass,
                deptId:deptId,
                userOaEmail:userOaEmail,
                userOrdinaryEmail:userOrdinaryEmail,
                userFullName:userFullName,
                userPhone:userPhone,
                userRight:userRight
            },
            type:"POST",
            dataType:"json",
            success:function(data){
                if(data.status == "ok"){
                    alert(data.des);
                    $('#addUser').modal('hide');
                }else{
                    alert(data.des);
                }
            },
            error:function(){

            }
        });

    }

    function uploadFile() {
        $.ajaxFileUpload({
            url: '<%=request.getContextPath() %>/admin/addUserByExcel',
            secureuri: false,// 一般设置为false
            fileElementId: "excelFile",// 文件上传表单的id <input type="file" id="fileUpload" name="file" />
            dataType: 'json',// 返回值类型 一般设置为json
            data: {},

            success: function (data) // 服务器成功响应处理函数
            {
                if(data.status =="empty"){
                    alert("请选择要导入的用户文件，后缀为xls！");
                }else if(data.status == "ok"){
                    alert("批量添加用户成功");
                    $('#addUserByExcel').modal('hide');
                }else if(data.status == "error"){
                    alert(data.des);
                }
            },
            error: function (xhr, type, errorThrown)// 服务器响应失败处理函数
            {
                console.log(type);
            }
        });
        return false;
    };

    $(document).ready(function(){
        if (adminId === null) {
            //window.location.href = hosturl+"/login.jsp";
        } else {
            searchUser();
        }
    });
</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/common.js"></script>
<!--[if lte IE 9]>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/placeholderie8.js"></script>
<![endif]-->