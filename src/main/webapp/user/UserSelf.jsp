<%--
  Created by IntelliJ IDEA.
  User: CMCC-Sunmo
  Date: 2016/9/19
  Time: 10:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>用户个人信息</title>
    <%--<!--[if !IE]>--%>
    <%--<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-3.1.0.min.js"></script>--%>
    <%--<![endif]-->--%>
    <%--<!--[if gt IE 9]>--%>
    <%--<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-3.1.0.min.js"></script>--%>
    <%--<![endif]-->--%>
    <%--<!--[if lte IE 9]>--%>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.12.2.min.js"></script>
    <%--<![endif]-->--%>

    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
    <!--[if lte IE 9]>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/respond.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/html5shiv.js"></script>
    <![endif]-->
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/md5.js"></script>
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/contract.css" />
</head>
<body>
<jsp:include page="${request.getContextPath() }/view/UserNav.jsp" flush="true" />
<div class="container mycontainer">
    <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-4">
            <%--<a class="btn btn-primary" href="<%=request.getContextPath() %>/file/download?fileName=ContractTemplet.xls">下载模板</a>--%>

            <button class="btn btn-primary" data-toggle="modal" data-target="#updateUserPass">修改密码</button>
			<button class="btn btn-primary" data-toggle="modal" data-target="#updateEmail">修改邮箱</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr><th>用户信息</th><th>内容</th></tr>
                </thead>
                <tr>
                    <td>用户名称</td>
                    <td id="userName"></td>
                </tr>
                <tr>
                    <td>用户部门</td>
                    <td id="userDept"></td>
                </tr>
                <tr>
                    <td>用户OA邮箱</td>
                    <td id="userOAEmail"></td>
                </tr>
                <tr>
                    <td>用户139邮箱</td>
                    <td id="userOraEmail"></td>
                </tr>
                <tr>
                    <td>用户姓名</td>
                    <td id="userFullName"></td>
                </tr>
                <tr>
                    <td>用户手机</td>
                    <td id="userPhone"></td>
                </tr>
                <tr>
                    <td>用户权限</td>
                    <td id="userRight"></td>
                </tr>
            </table>
        </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
	<div class="modal fade" id="updateEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" >更新邮箱</h4>
            </div>
            <div class="modal-body">
				邮箱类型：<select id="email_select" class="form-control">
                        <option value="1" >OA邮箱</option>
                        <option value="2" >139邮箱</option>
                </select>
				邮箱：<input required id="email" class="form-control" type="text" placeholder="输入您的邮箱">
			</div>
			<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" id="email_sub" onclick="" class="btn btn-primary" >更新</button>
            </div>
		</div>
	</div>
</div>
<div class="modal fade" id="updateUserPass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">修改密码</h4>
            </div>
            <div class="modal-body">
                <%--<div class="row">--%>
                    <%--<div class="center-block">--%>
                <form class="" role="form">
                    <input required id="oldPass" class="form-control" type="password" placeholder="输入旧密码">
                    <input required id="newPass1" class="form-control" type="password" placeholder="输入新密码">
                    <input required id="newPass2" class="form-control" type="password" placeholder="确认新密码">
                </form>
                    <%--</div>--%>
                <%--</div>--%>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="updateUserPass()">提交</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<script>
    var userId = "<%=session.getAttribute("userId")%>";
    var userFullName = "<%=session.getAttribute("userFullName")%>";
    var deptId = "<%=session.getAttribute("deptId")%>";
    var userRight = "<%=session.getAttribute("userRight")%>";
    var dept = <%=session.getAttribute("DepartmentList")%>;
    var deptName = "";
	
	var oaEmail = "";
	var email139 = "";
	
    $.each(dept,function(index,item){
//        console.log(item.deptId);
        if(item.deptId == deptId){
            deptName = item.deptName;
        }

    });
	
	/*忽略缓存*/
	$.ajaxSetup({cache:false});
	
    $(document).ready(function(){

        if (userId === null) {
            //window.location.href = hosturl+"/login.jsp";
        } else {
            var aj = $.ajax( {
                url:'<%=request.getContextPath() %>/user/getSelfInfo',
                data:{
                    userId:userId
                },
                type:'GET',
                dataType:'json',
                success:function(data) {
                    if(data.status =="ok" ){
                        var user = data.data;
                        $("#userName").text(user.userName);
                        $("#userDept").text(deptName);
                        $("#userFullName").text(user.userFullName==null?"":user.userFullName);
                        $("#userOAEmail").text(user.userOaEmail==null?"":user.userOaEmail);
                        $("#userOraEmail").text(user.userOrdinaryEmail==null?"":user.userOrdinaryEmail);
                        $("#userPhone").text(user.userPhone==null?"":user.userPhone);

						
						oaEmail = user.userOaEmail + "";
						email139 = user.userOrdinaryEmail;
						$("#email").val(oaEmail);
						$("#email_sub").attr("onclick","updateEmail(" + "1" + ")");

                        var userQuality = 0;
                        switch (user.userRight){
                            case 1: userQuality = "总经理";break;
                            case 2: userQuality = "部门经理";break;
                            case 3: userQuality = "员工";break;
                        }

                        $("#userRight").text(userQuality)
                    }else{

                    }
                },
                error : function() {
                    alert("用户数据异常，请重新登录！");
                }
            });
        }
		
		
		$("#email_select").change(function(){
			var p = $(this).children('option:selected').val();
			if(p == "1"){
				$("#email").val(oaEmail);
				$("#email_sub").attr("onclick","updateEmail(" + "1" + ")");
			}else if(p == "2"){
				$("#email").val(email139);
				$("#email_sub").attr("onclick","updateEmail(" + "2" + ")");
			}
		});
    });

    function updateUserPass(){
        var oldPass = $("#oldPass").val();
        var newPass1 = $("#newPass1").val();
        var newPass2 = $("#newPass2").val();
        if(oldPass==""){
            alert("旧密码不能为空");
            $("#oldPass").focus();
            return;
        }
        if(newPass1==""){
            alert("新密码不能为空");
            $("#newPass1").focus();
            return;
        }
        if(newPass2==""){
            alert("确认密码不能为空");
            $("#newPass2").focus();
            return;
        }
        if(newPass1!=newPass2){
            alert("新密码和确认密码不同，请重新输入");
            $("#newPass1").focus();
            return;
        }
        var aj = $.ajax( {
            url:'<%=request.getContextPath() %>/user/updatePass',
            data:{
                userId:userId,
                oldPassword:hex_md5(oldPass),
                newPassword:hex_md5(newPass1)
            },
            type:'POST',
            dataType:'json',
            success:function(data) {
                if(data.status =="ok" ){
                    alert(data.des);
                    $('#updateUserPass').modal('hide')
                }else{
                    alert(data.des);
                }
            },
            error : function() {
                alert("修改失败，请刷新后重试！");
            }
        });

    }
	
/*更新邮箱*/
function updateEmail(type_id){
	var t = type_id + "";
	var type = "";
	if(t == "1"){
		type = "oa";
	}else if(t == "2"){
		type = "139";
	}
	var email = $("#email").val();
	//正则匹配
	var strReg=/^\w+((-\w+)|(\.\w+))*\@{1}\w+\.{1}\w{2,4}(\.{0,1}\w{2}){0,1}/ig;
	if(email.search(strReg) == -1) {
	  alert("邮箱格式错误");
		return false;
	}
	
	var aj = $.ajax({
		url:'<%=request.getContextPath() %>/user/updateEmail',
		data:{
			email:email,
			type:type,
			userId:userId
		},
		type:'POST',
        dataType:'json',
        success:function(data) {
          if(data.status =="ok" ){
             alert(data.des);
             $('#updateEmail').modal('hide')
          }else{
             alert(data.des);
          }
        },
        error : function() {
          alert("异常！");
        }
	});
	setTimeout(function(){window.location.reload(true);},1000);
};

</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/common.js"></script>
<!--[if lte IE 9]>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/placeholderie8.js"></script>
<![endif]-->
</body>
</html>

