<%--
  Created by IntelliJ IDEA.
  User: CMCC-Sunmo
  Date: 2016/9/19
  Time: 10:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container mycontainer">
    <div class="panel panel-default">
        <div class="panel-heading">搜索条件</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-2">
                    <select id="dept" class="form-control">
                    </select>
                </div>
                <div class="col-md-2">
                    <input id="userName" type="text" class="form-control" placeholder="用户名称">
                </div>
                <div class="col-md-2">
                    <input id="contractSer" type="text" class="form-control" placeholder="合同编号">
                </div>
                <div class="col-md-2">
                    <input id="contractName" type="text" class="form-control" placeholder="合同名称">
                </div>
                <div class="col-md-2">
                    <select id="contractCate" class="form-control">
                        <option value="0">所有合同</option>
                        <option value="1">呈批件</option>
                        <option value="2">审批表</option>
                        <option value="3">合同</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary" onclick="searchContract();">搜索</button>
                    <label id="dataLen">共有？条</label>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">搜索结果</div>
        <div class="panel-body">
            <div class="row">
                <table id="thead" class="table table-bordered table-striped table-hover">
                    <colgroup>
                        <col style="">
                        <col style="">
                        <col style="width:150px">
                        <col style="">
                        <col style="width:50px">
                        <col style="width:50px">
                        <col style="">
                        <col style="">
                        <col style="width:50px">
                        <col style="width:100px">
                        <col style="width:100px">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>合同编号</th>
                        <th>合同名称</th>
                        <th>部门</th>
                        <th>用户名</th>
                        <th>合同类型</th>
                        <th>付款期数</th>
                        <th>开始时间</th>
                        <th>结束时间</th>
                        <th>是否提醒</th>
                        <th>提醒时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody id="data-container">
                    <%--<div id="data-container"></div>--%>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div id="wrapper">
                    <section>
                        <div id="pagination-container"></div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="updatePeriod" backdrop="static"  keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
	<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" >修改合同分期</h4>
            </div>
            <div class="modal-body">
				<div class="row">
					<div class="col-md-3">
						<h6 class="modal-title" >付款时间：</h6>
					</div>
					<div class="col-md-2">
						<h6 class="modal-title" >付款金额：</h6>
					</div>
					<div class="col-md-1">
						<h6 class="modal-title" >Tip</h6>
					</div>
					<div class="col-md-3">
						<h6 class="modal-title" >提醒时间：</h6>
					</div>
					<div class="col-md-2">
						<h6 class="modal-title" >操作</h6>
					</div>
				</div>
				<div  id="per_row">
<!--
					<div class="col-md-3"><input required id="" class="form-control" type="date" placeholder="付款时间"></div>
					<div class="col-md-3"><input required id="" class="form-control" type="text" placeholder="付款金额"></div>
					<div class="col-md-3"><input required id="" class="form-control" type="date" placeholder="提醒时间"></div>
					<div class="col-md-3"><button type="button" id="per_sub" class="btn btn-primary" onclick="updatePeriod()">提交</button></div>
-->
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="addConByExcel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel1">批量上传合同</h4>
            </div>
            <div class="modal-body">
				<input required id="excelFile" class="btn" type="file"  name="file" size="15" accept=".xls" placeholder="上传Excel文件" enctype="multipart/form-data" maxlength="100">
			</div>
			<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" id="excel_sub" class="btn btn-primary" onclick="addContractByExcel()">提交</button>
            </div>
		</div>
	</div>
</div>
<div class="modal fade" id="updateContract" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel2">修改合同</h4>
            </div>
            <div class="modal-body">
                <%--<div class="row">--%>
                    <%--<div class="center-block">--%>
                <form class="" role="form">
					合同类型：<select id="uContractCate" class="form-control">
                        <option value="1" >呈批件</option>
                        <option value="2" >审批表</option>
                        <option value="3" >合同</option>
                    </select>
					<input type="hidden" id="hideId"/>
                    合同编号：<input required id="uContractSerial"  class="form-control" type="text" placeholder="输入合同编号">
                    合同名称：<input required id="uContractName" class="form-control" type="text" placeholder="输入合同名称">
						起始时间：<input required id="uStartTime" onfocus="calendar()" class="form-control" placeholder="输入合同开始时间">
						结束时间：<input required id="uEndTime" onfocus="calendar()" class="form-control" placeholder="输入合同结束时间">
						<div id="peridNum_row">
						分期次数：<input required id="uPeriodNum" class="form-control" type="text" placeholder="输入合同分期次数">
						</div>
						是否提醒：<input id="uCtIsWarn" type="checkbox" /><br/>
						<div id="reTime_row">
						提醒时间：<input required id="uRemindTime" onfocus="calendar()" class="form-control" placeholder="输入合同提醒时间">
						</div>
                </form>
                    <%--</div>--%>
                <%--</div>--%>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" id="contract_sub" class="btn btn-primary" onclick="">提交</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<script>
    var userId = "<%=session.getAttribute("userId")%>";
    var userFullName = "<%=session.getAttribute("userFullName")%>";
    var deptId = "<%=session.getAttribute("deptId")%>";
    var userRight = "<%=session.getAttribute("userRight")%>";
    var dept = <%=session.getAttribute("DepartmentList")%>;
    var deptName = "";
    var deptOption = "";
	if(userRight == "1"){
		deptOption = '<option value="0">所有部门</option>';
		$.each(dept,function(index,item){
        	deptOption+='<option value="'+item.deptId+'">'+item.deptName+'</option>';
    	});
	}else if(userRight == "2"){
		$.each(dept,function(index,item){
			if(deptId == item.deptId){
				deptOption+='<option value="'+item.deptId+'">'+item.deptName+'</option>';	
			}      	
    	});
	}
    $("#dept").html($("#dept").html()+deptOption);


    function searchContract() {
        var userName = $("#userName").val();
        var contractSer = $("#contractSer").val();
        var contractName = $("#contractName").val();
        var contractCate = $("#contractCate").val();
		var deptId = $("#dept").val();
        var aj = $.ajax( {
            url:'<%=request.getContextPath() %>/user/findContract',
            data:{
                deptId:deptId,
                userId:userName,
                contractSerial:contractSer,
                contractName:contractName,
                contractCate:contractCate
            },
            type:'POST',
            dataType:'json',
            success:function(data) {
				contracts = data;
                if(data.status =="ok" ){
                    document.getElementById("dataLen").innerHTML="共有"+data.data.length +"条";
					$('#data-container').html("");
					$('#pagination-container').html("");
                    $('#pagination-container').pagination({
                        dataSource: data.data,
                        pageSize: 20,
                        showGoInput: true,
                        showGoButton: true,
                        autoHidePrevious: true,
                        autoHideNext: true,
//                        showPrevious: false,
//                        showNext: false,
//                        showPageNumbers: false,
//                        showNavigator: true,
                        pageNumber: 1,
                        callback: function (data, pagination) {
                            // template method of yourself
                            var html = simpleTemplating(data);
                            $('#data-container').html(html);
                        }
                    });
                }else{
					document.getElementById("dataLen").innerHTML="共有0条";
					$('#data-container').html("");
					$('#pagination-container').html("");
                }
            },
            error : function() {
                alert("异常！");
            }
        });
    }
	
</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/lib/contract.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/common.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/calendar.js"></script>
<!--[if lte IE 9]>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/placeholderie8.js"></script>
<![endif]-->