<%--
  Created by IntelliJ IDEA.
  User: CMCC-Sunmo
  Date: 2016/9/18
  Time: 17:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>用户合同管理</title>
    <%--<!--[if !IE]>--%>
    <%--<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-3.1.0.min.js"></script>--%>
    <%--<![endif]-->--%>
    <%--<!--[if gt IE 9]>--%>
    <%--<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-3.1.0.min.js"></script>--%>
    <%--<![endif]-->--%>
    <%--<!--[if lte IE 9]>--%>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.12.2.min.js"></script>
	<link href="<%=request.getContextPath() %>/css/bootstrap-ie8.css" rel="stylesheet">
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/html5shiv.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/respond.js"></script>
     <%--<![endif]-->--%>

    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/pagination.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/lib/util.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/ajaxfileupload.js"></script>
	
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/lib/util.js"></script>

    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.easyui.min.js"></script>

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/easyui.css">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/icon.css">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/demo.css">

    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/contract.css" />
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/pagination.css" />
</head>
<body>
<%--<jsp:include page="../view/header.jsp" flush="true" />--%>
<jsp:include page="${request.getContextPath() }/view/UserNav.jsp" flush="true" />
<jsp:include page="${request.getContextPath() }/user/Contract.jsp" flush="true" />
<%--<jsp:include page="../view/footer.jsp" flush="true" />--%>
</body>
</html>
