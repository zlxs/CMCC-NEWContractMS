<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>登录</title>
    <%--<!--[if !IE]>--%>
    <%--<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-3.1.0.min.js"></script>--%>
    <%--<![endif]-->--%>
    <%--<!--[if gt IE 9]>--%>
    <%--<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-3.1.0.min.js"></script>--%>
    <%--<![endif]-->--%>
    <%--<!--[if lte IE 9]>--%>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.12.2.min.js"></script>
    <%--<![endif]-->--%>

    <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
    <!--[if lte IE 9]>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/respond.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/html5shiv.js"></script>
    <![endif]-->
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/md5.js"></script>
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/signin.css" />
</head>
<body>

<div class="container" style="padding-top:10%">

    <form class="form-signin" role="form">
        <h2 class="form-signin-heading" style="margin:0,auto;padding-left:60px">合同管理系统</h2>
        用户名<input type="text" class="form-control"  required autofocus id="userName">
        密&nbsp&nbsp码<input type="password" class="form-control"  required id="userPass" onkeydown="keyLogin()">
        <%--<div class="checkbox">--%>
            <%--<label>--%>
                <%--<input type="checkbox" value="remember-me"> Remember me--%>
            <%--</label>--%>
        <%--</div>--%>
        <button class="btn btn-lg btn-primary btn-block" type="button" onclick="login()">登录</button>
    </form>
    <%--<!--[if lte IE 9]>--%>
    <%--<h4 style="text-align: center;color: red">如果您安装过GoogleChromeFrame,请忽略此消息。</h4>--%>
    <%--<hr />--%>
    <%--<h4 style="text-align: center;color: red">如果您使用的是非Chrome浏览器，特别是IE6,7,8,9浏览器，为了获得更好的界面体验，请下载并安装<a href="<%=request.getContextPath() %>/file/download?fileName=GoogleChromeframe.msi">GoogleChromeFrame</a>！</h4>--%>
    <%--<![endif]-->--%>
</div> <!-- /container -->
<script type="text/javascript">

    function keyLogin() {
        if(event.keyCode==13){
            login();
        }
    }
    function login(){
        var userName = $("#userName").val();
        var userPass = $("#userPass").val();
        if(userName==""){
            alert("用户名不能为空！");
            $("#userName").focus();
            return;
        }
        if(userPass==""){
            alert("密码不能为空！");
            $("#userPass").focus();
            return;
        }
        var aj = $.ajax( {
            url:'<%=request.getContextPath() %>/user/login',
            data:{
                userName:userName,
                userPass:hex_md5(userPass)
            },
            type:'POST',
            dataType:'json',
            success:function(data) {
                if(data.status =="ok" ){
                    if(data.des=="user") {
                        window.location.href = "<%=request.getContextPath() %>/user/UserContract.jsp";
                    }else if(data.des=="admin"){
                        window.location.href = "<%=request.getContextPath() %>/admin/AdminContract.jsp";
                    }
                }else{
                    alert(data.des);
                }
            },
            error : function() {
                alert("登录失败！请联系管理员解决！");
            }
        });

    }
</script>
</body>
</html>