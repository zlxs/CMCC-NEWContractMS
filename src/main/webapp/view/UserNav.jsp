<%--
  Created by IntelliJ IDEA.
  User: CMCC-Sunmo
  Date: 2016/9/19
  Time: 10:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/navbar-static-top.css" />
<nav class="navbar navbar-default navbar-static-top navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">合同管理系统</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li id="navOfUserContract"><a href="<%=request.getContextPath() %>/user/UserContract.jsp">合同管理</a></li>
                <li id="navOfUserSelf"><a href="<%=request.getContextPath() %>/user/UserSelf.jsp">个人信息</a></li>
                <li id="navOfDeptContract"><a href="<%=request.getContextPath() %>/user/DeptContract.jsp">部门合同</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a id="navOfuserName">欢迎 </a></li>
                <li><a href="javascript:logout()">注销</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<script type="text/javascript">
	
	var userId = "<%=session.getAttribute("userId")%>";
    var userFullName = "<%=session.getAttribute("userFullName")%>";
    var deptId = "<%=session.getAttribute("deptId")%>";
    var userRight = "<%=session.getAttribute("userRight")%>";
	
    var hostname = window.location.href;
    var flag1=hostname.indexOf("UserContract");
    var flag2=hostname.indexOf("UserSelf");
    var flag3=hostname.indexOf("DeptContract");
    if(flag1>0){
        $("#navOfUserContract").addClass("active");
        $("#navOfUserSelf").removeClass("active");
        $("#navOfDeptContract").removeClass("active");
    }
    if(flag2>0){
        $("#navOfUserContract").removeClass("active");
        $("#navOfUserSelf").addClass("active");
        $("#navOfDeptContract").removeClass("active");
    }
    if(flag3>0){
        $("#navOfUserContract").removeClass("active");
        $("#navOfUserSelf").removeClass("active");
        $("#navOfDeptContract").addClass("active");
    }
	
	$(document).ready(function(){
		$("#navOfuserName").text("欢迎 " + userFullName);
		if(userRight == "3"){
			$("#navOfDeptContract").hide();
		}
    });
	
	function logout(){
		var aj = $.ajax({
			url:'<%=request.getContextPath() %>/user/logout',
			data:{
				
			},
			type:'GET',
            dataType:'json',
			success:function(data){
				if(data.status == "ok"){
					window.location.href = "<%=request.getContextPath() %>/login.jsp";
				}else{
					alert(data.des);
				}
			}
		})
	}
	
</script>