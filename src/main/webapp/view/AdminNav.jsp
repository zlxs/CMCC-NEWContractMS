<%--
  Created by IntelliJ IDEA.
  User: CMCC-Sunmo
  Date: 2016/9/19
  Time: 10:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/navbar-static-top.css" />
<nav class="navbar navbar-default navbar-static-top navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">合同管理系统</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li id="navOfAdminContract"><a href="<%=request.getContextPath() %>/admin/AdminContract.jsp">合同管理</a></li>
                <li id="navOfAdminUser"><a href="<%=request.getContextPath() %>/admin/AdminUser.jsp">用户管理</a></li>
                <li id="navOfAdminLog"><a href="<%=request.getContextPath() %>/admin/AdminLog.jsp">日志管理</a></li>
                <li id="navOfAdminSelf"><a href="<%=request.getContextPath() %>/admin/AdminSelf.jsp">个人信息</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a id="navOfAdminName">欢迎</a></li>
                <li><a href="javascript:logout()">注销</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<script type="text/javascript">
	
	var userId = "<%=session.getAttribute("adminId")%>";
    var adminFallName = "<%=session.getAttribute("adminFallName")%>";
	var adminName = "<%=session.getAttribute("adminName")%>";
    var deptId = "<%=session.getAttribute("deptId")%>";
    var userRight = "<%=session.getAttribute("userRight")%>";
	
    var hostname = window.location.href;
    var flag1=hostname.indexOf("AdminContract");
    var flag2=hostname.indexOf("AdminUser");
    var flag3=hostname.indexOf("AdminLog");
    var flag4=hostname.indexOf("AdminSelf");

    if(flag1>0){
        $("#navOfAdminContract").addClass("active");
        $("#navOfAdminUser").removeClass("active");
        $("#navOfAdminLog").removeClass("active");
        $("#navOfAdminSelf").removeClass("active");
    }
    if(flag2>0){
        $("#navOfAdminContract").removeClass("active");
        $("#navOfAdminUser").addClass("active");
        $("#navOfAdminLog").removeClass("active");
        $("#navOfAdminSelf").removeClass("active");
    }
    if(flag3>0){
        $("#navOfAdminContract").removeClass("active");
        $("#navOfAdminUser").removeClass("active");
        $("#navOfAdminLog").addClass("active");
        $("#navOfAdminSelf").removeClass("active");
    }
    if(flag4>0){
        $("#navOfAdminContract").removeClass("active");
        $("#navOfAdminUser").removeClass("active");
        $("#navOfAdminLog").removeClass("active");
        $("#navOfAdminSelf").addClass("active");
    }
	
	$(document).ready(function(){
		$("#navOfAdminName").text("欢迎 " + adminName);
    });
	
	function logout(){
		var aj = $.ajax({
			url:'<%=request.getContextPath() %>/user/logout',
			data:{
				
			},
			type:'GET',
            dataType:'json',
			success:function(data){
				if(data.status == "ok"){
					window.location.href = "<%=request.getContextPath() %>/login.jsp";
				}else{
					alert(data.des);
				}
			}
		})
	}
</script>